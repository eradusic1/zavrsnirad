// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        jcenter()
        
    }
    dependencies {
        classpath ("com.android.tools.build:gradle:3.3.2")
        classpath ("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.72")

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}
plugins{
    `kotlin-dsl`
}
allprojects {
    repositories {
        google()
        jcenter()
        
    }
}
/*
tasks.register("clean",Delete::class){
    delete(rootProject.buildDir)
}
*/
