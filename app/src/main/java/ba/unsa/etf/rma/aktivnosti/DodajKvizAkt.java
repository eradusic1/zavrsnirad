package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.broadcastreceiveri.InternetBroadcastReceiver;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.KategorijeAdapter;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.PitanjaAdapter;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.RangListaObjekat;
import ba.unsa.etf.rma.korisneklase.OsvjezivacPodatakaUBazama;
import ba.unsa.etf.rma.resultreceiveri.DodavanjeKvizaReceiver;
import ba.unsa.etf.rma.resultreceiveri.MijenjanjeKvizaPoIdjuReceiver;
import ba.unsa.etf.rma.resultreceiveri.ProvjeraKvizaPoNazivuReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeKategorijaReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeKvizaPoIdjuReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjePitanjaReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeRanglistePoNazivuKvizaReceiver;
import ba.unsa.etf.rma.servisi.DodavanjeElementaURanglistuService;
import ba.unsa.etf.rma.servisi.DodavanjeKvizaService;
import ba.unsa.etf.rma.servisi.MijenjanjeKvizaPoIdjuService;
import ba.unsa.etf.rma.servisi.ProvjeraKvizaPoNazivuService;
import ba.unsa.etf.rma.servisi.UcitavanjeKategorijaService;
import ba.unsa.etf.rma.servisi.UcitavanjeKvizaPoIdjuService;
import ba.unsa.etf.rma.servisi.UcitavanjePitanjaService;
import ba.unsa.etf.rma.servisi.UcitavanjeRanglistePoNazivuKvizaService;

public class DodajKvizAkt extends AppCompatActivity implements ProvjeraKvizaPoNazivuReceiver.Receiver,
        UcitavanjeKvizaPoIdjuReceiver.Receiver, UcitavanjeKategorijaReceiver.Receiver, DodavanjeKvizaReceiver.Receiver,
        MijenjanjeKvizaPoIdjuReceiver.Receiver, UcitavanjePitanjaReceiver.Receiver, UcitavanjeRanglistePoNazivuKvizaReceiver.Receiver,
        InternetBroadcastReceiver.Aktivnost {

    private static int dozvoljeniIdKategorijeImportovanog;
    private ArrayList<Kategorija> listaKategorija = new ArrayList<>();
    private ArrayList<Pitanje> listaPitanjaUKvizu = new ArrayList<>();
    private ArrayList<Pitanje> listaPitanjaVanKviza = new ArrayList<>();
    private ArrayList<Kviz> listaDosadasnjihKvizova = new ArrayList<>();
    private Kviz kvizZaImportovanje = new Kviz();
    private Kviz povratniKviz = new Kviz();
    private int pozicijaKvizaZaUredjivanje;
    private int pozicijaOdabraneKategorije;
    private boolean validnoImportovanjeKviza;
    private boolean validnaForma;
    private Spinner spKategorije;
    private EditText etNaziv;
    private ListView lvDodanaPitanja;
    private ListView lvMogucaPitanja;
    private Button btnImportKviz;
    private Button btnDodajKviz;
    private View footerDodajPitanje;
    private float velicinaSlovaEditTexta;
    private int paddingEditTexta;
    private BaseAdapter kategorijeAdapter;
    private BaseAdapter mogucaPitanjaAdapter;
    private BaseAdapter dodanaPitanjaAdapter;
    private boolean dodavanjeNovogKviza;
    private String prijasnjiNazivKviza = null;
    private RangListaObjekat rangListaKviza = new RangListaObjekat();
    private InternetBroadcastReceiver internetBroadcastReceiver = new InternetBroadcastReceiver();
    private IntentFilter internetIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    private boolean imaInternetKonekcije;
    private boolean prvo;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodaj_kviz_akt);
        progressDialog = new ProgressDialog(DodajKvizAkt.this);
        dobaviWidgete();
        postaviFooterNaListuPitanja();
        dobaviDimenzijeEditTexta();
        pokupiPoslanePodatke();
    }

    private void dobaviWidgete() {
        btnImportKviz = (Button) findViewById(R.id.btnImportKviz);
        btnDodajKviz = (Button) findViewById(R.id.btnDodajKviz);
        spKategorije = (Spinner) findViewById(R.id.spKategorije);
        etNaziv = (EditText) findViewById(R.id.etNaziv);
        lvMogucaPitanja = (ListView) findViewById(R.id.lvMogucaPitanja);
        lvDodanaPitanja = (ListView) findViewById(R.id.lvDodanaPitanja);
        footerDodajPitanje = ((LayoutInflater) DodajKvizAkt.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.footer_dodaj_pitanje, null, false);
    }

    private void postaviFooterNaListuPitanja() {
        lvDodanaPitanja.addFooterView(footerDodajPitanje);
    }

    private void dobaviDimenzijeEditTexta() {
        paddingEditTexta = etNaziv.getPaddingBottom();
        velicinaSlovaEditTexta = etNaziv.getTextSize();
    }

    private void pokupiPoslanePodatke() {
        inicijalizirajKategorije();
    }

    private void inicijalizirajKategorije() {
        listaKategorija = new ArrayList<>();
        Kategorija kategorijaSvi = new Kategorija();

        // Univerzalna kategorija "Svi" koja sluzi za prikaz svih kvizova, pa i onih koji nemaju kategoriju

        kategorijaSvi.setNaziv(getResources().getString(R.string.specijalna_kategorija_svi));
        kategorijaSvi.setId("976");
        kategorijaSvi.setIdIzFirebasea("ktgrjSvi");
        listaKategorija.add(kategorijaSvi);
        ucitajKategorijeIzFireBaseaIOsvjeziEkran();
    }

    private void ucitajKategorijeIzFireBaseaIOsvjeziEkran() {
        UcitavanjeKategorijaReceiver ucitavanjeKategorijaReceiver = new UcitavanjeKategorijaReceiver(new Handler());
        ucitavanjeKategorijaReceiver.setReceiver(this);
        Intent intent = new Intent(this, UcitavanjeKategorijaService.class);
        intent.putExtra("extraUcitavanjeKategorijaReceiver", ucitavanjeKategorijaReceiver);
        startService(intent);
    }

    private void onemoguciImportAkoJeMijenjanje() {
        if (!getIntent().getBooleanExtra("extraDodavanjeNovogKviza", false)) {
            btnImportKviz.setEnabled(false);
        }
    }

    private void postaviAdaptere() {
        kategorijeAdapter = new KategorijeAdapter(this, listaKategorija, getResources());
        spKategorije.setAdapter(kategorijeAdapter);
        mogucaPitanjaAdapter = new PitanjaAdapter(this, listaPitanjaVanKviza, getResources());
        lvMogucaPitanja.setAdapter(mogucaPitanjaAdapter);
        dodanaPitanjaAdapter = new PitanjaAdapter(this, listaPitanjaUKvizu, getResources());
        lvDodanaPitanja.setAdapter(dodanaPitanjaAdapter);
    }

    private void popuniWidgetePostojecimPodacima() {
        etNaziv.setText(povratniKviz.getNaziv());
        if (!getIntent().getBooleanExtra("extraDodavanjeNovogKviza", false)) {
            int pozicijaKategorije = 0;
            for (int i = 0; i < listaKategorija.size(); i++) {
                if (listaKategorija.get(i).getNaziv().equals(povratniKviz.getKategorija().getNaziv())) {
                    pozicijaKategorije = i;
                    break;
                }
            }
            spKategorije.setSelection(pozicijaKategorije);
            pozicijaOdabraneKategorije = pozicijaKategorije;
        }
        else {
            spKategorije.setSelection(0);
            pozicijaOdabraneKategorije = 0;
        }
    }

    private void inicijalizirajDozvoljeniIdKategorijeImportovanog() {
        dozvoljeniIdKategorijeImportovanog = 976;
    }

    private void postaviListenerZaSkrolanjeListeDodanihPitanja() {
        lvDodanaPitanja.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    private void postaviListenerZaSkrolanjeListeMogucihPitanja() {
        lvMogucaPitanja.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    private void postaviListenerNaKategoriju() {
        spKategorije.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == listaKategorija.size() - 1) {

                    // Ako je kliknut posljednji element spinnera, a to je dodavanje nove kategorije

                    pokreniDodavanjeNoveKategorije();
                }
                else {

                    // Inace se odabire kliknuta kategorija i dodjeljuje trenutnom kvizu

                    pozicijaOdabraneKategorije = position;
                    povratniKviz.setKategorija(listaKategorija.get(pozicijaOdabraneKategorije));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                spKategorije.setSelection(pozicijaOdabraneKategorije);
            }
        });
    }

    private void pokreniDodavanjeNoveKategorije() {
        Intent intentDodavanjeKategorije = new Intent(DodajKvizAkt.this, DodajKategorijuAkt.class);
        intentDodavanjeKategorije.putParcelableArrayListExtra("extraListaDosadasnjihKategorija", listaKategorija);
        startActivityForResult(intentDodavanjeKategorije, 3);
    }

    private void postaviListenerNaDugmeImportovanjeKviza() {
        btnImportKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentOtvaranjeDokumenta = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intentOtvaranjeDokumenta.addCategory(Intent.CATEGORY_OPENABLE);
                String[] tipovi = {"text/plain", "text/csv"};
                intentOtvaranjeDokumenta.setType("*/*");
                intentOtvaranjeDokumenta.putExtra(Intent.EXTRA_MIME_TYPES, tipovi);
                startActivityForResult(intentOtvaranjeDokumenta, 5);
            }
        });
    }

    private void postaviListenerNaDugmeDodavanjeKviza() {
        btnDodajKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validirajFormu();
                if (!dodavanjeNovogKviza) {
                    if (etNaziv.getText().toString().equals(prijasnjiNazivKviza)) {
                        if (!validnaForma) return;
                        Intent intentPovratakNaKvizove = new Intent();
                        if (!getIntent().getBooleanExtra("extraDodavanjeNovogKviza", false)) {
                            intentPovratakNaKvizove.putExtra("extraPozicijaPovratnogKviza",
                                    getIntent().getIntExtra("extraPozicijaKvizaUListi", 0));
                        }
                        povratniKviz.setNaziv(etNaziv.getText().toString());
                        povratniKviz.setPitanja(listaPitanjaUKvizu);
                        povratniKviz.setKategorija(listaKategorija.get(spKategorije.getSelectedItemPosition()));
                        izmijeniPostojeciKvizPoIdjuUFirebaseu(povratniKviz);
                        intentPovratakNaKvizove.putExtra("extraPovratniKviz", (Serializable) povratniKviz);
                        intentPovratakNaKvizove.putExtra("extraPozicijaPovratnogKviza", pozicijaKvizaZaUredjivanje);
                        intentPovratakNaKvizove.putParcelableArrayListExtra("extraUredjenaListaKategorija", listaKategorija);
                        setResult(RESULT_OK, intentPovratakNaKvizove);
                        finish();
                    }
                    else provjeriPostojanjeKvizaUFirestoreu(etNaziv.getText().toString());
                }
                else {
                    provjeriPostojanjeKvizaUFirestoreu(etNaziv.getText().toString());
                }
            }
        });
    }

    private void validirajFormu() {
        validnaForma = true;
        boolean zacrvenitiEtNaziv = false;
        if (jeLiPrazanNazivKviza()) {
            zacrvenitiEtNaziv = true;
            validnaForma = false;
        }
        if (zacrvenitiEtNaziv) etNaziv.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
        else resetujPozadinuEtNaziv();
    }

    private boolean jeLiPrazanNazivKviza() {
        return etNaziv.getText().toString().trim().length() == 0;
    }

    private void resetujPozadinuEtNaziv() {
        etNaziv.setBackgroundResource(android.R.drawable.editbox_background_normal);
        etNaziv.setTextSize(velicinaSlovaEditTexta / 3);
        etNaziv.setPadding(paddingEditTexta, paddingEditTexta, paddingEditTexta, paddingEditTexta);
    }

    private void provjeriPostojanjeKvizaUFirestoreu(String nazivKviza) {
        ProvjeraKvizaPoNazivuReceiver provjeraKvizaPoNazivuReceiver = new ProvjeraKvizaPoNazivuReceiver(new Handler());
        provjeraKvizaPoNazivuReceiver.setReceiver(this);
        Intent intent = new Intent(this, ProvjeraKvizaPoNazivuService.class);
        intent.putExtra("extraProvjeraKviPoNazivuReceiver", provjeraKvizaPoNazivuReceiver);
        intent.putExtra("extraNazivZadanogKviza", nazivKviza);
        startService(intent);
    }

    private void postaviListenerNaFooterDodavanjePitanja() {
        footerDodajPitanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDodavanjePitanja = new Intent(DodajKvizAkt.this, DodajPitanjeAkt.class);
                intentDodavanjePitanja.putParcelableArrayListExtra("extraListaDosadasnjihDodanihPitanja", listaPitanjaUKvizu);
                intentDodavanjePitanja.putParcelableArrayListExtra("extraListaDosadasnjihMogucihPitanja", listaPitanjaVanKviza);
                startActivityForResult(intentDodavanjePitanja, 4);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {

            /* Ako se korisnik vratio pritiskom na Back spinner treba ostati u stanju u kom je bio
            prije odlaska u drugu aktivnost */

            spKategorije.setSelection(pozicijaOdabraneKategorije);
        }
        if (requestCode == 3) {

            /* Slucaj kada je aktivnost DodajKategorijuAkt pozvana i vracena nazad na ovu s ciljem dodavanja
            nove kategorije */

            if (resultCode == RESULT_OK) {
                dodajNovuKategoriju((Kategorija) data.getSerializableExtra("extraPovratnaKategorija"));
                pozicijaOdabraneKategorije = listaKategorija.size() - 2;
                osvjeziPrikazKategorija();
                spKategorije.setSelection(pozicijaOdabraneKategorije);
            }
        }
        else if (requestCode == 4) {

            /* Slucaj kada je aktivnost DodajPitanjeAkt pozvana i vracena nazad na ovu s ciljem dodavanja
            novog pitanja */

            if (resultCode == RESULT_OK) {
                dodajNovoPitanje((Pitanje) data.getSerializableExtra("extraPovratnoPitanje"));
                osvjeziPrikazDodanihPitanja();
            }
        }
        else if (requestCode == 5) {

            /* Slucaj kada je bilo pokrenuto importovanje kviza iz tekstualne datoteke */

            if (resultCode == RESULT_OK) {
                Uri identifikator = null;
                if (data != null) {
                    identifikator = data.getData();
                    validnoImportovanjeKviza = true;
                    iscitavajKvizIzDatoteke(identifikator);
                }
            }
        }
    }

    private void dodajNovuKategoriju(Kategorija novaKategorija) {
        int odredisnaPozicija = listaKategorija.size() - 1;
        listaKategorija.add(odredisnaPozicija, novaKategorija);
    }

    private void osvjeziPrikazKategorija() {
        kategorijeAdapter = new KategorijeAdapter(this, listaKategorija, getResources());
        spKategorije.setAdapter(kategorijeAdapter);
    }

    private void dodajNovoPitanje(Pitanje novoPitanje) {
        listaPitanjaUKvizu.add(novoPitanje);
        povratniKviz.setPitanja(listaPitanjaUKvizu);
    }

    private void osvjeziPrikazDodanihPitanja(){
        dodanaPitanjaAdapter.notifyDataSetChanged();
    }

    private void osvjeziPrikazMogucihPitanja(){
        mogucaPitanjaAdapter.notifyDataSetChanged();
    }

    private void iscitavajKvizIzDatoteke(Uri identifikator){
        kvizZaImportovanje = new Kviz();
        BufferedReader citac = null;
        try {
            citac = dajNoviCitacDatoteke(identifikator);
            if (citac == null) return;
            String redDatoteke = "";
            boolean prviRed = true;
            String prviRedDatoteke = "";
            int brojRedovaPoslijePrvog = 0;
            while ((redDatoteke = citac.readLine()) != null) {
                if (prviRed) {
                    prviRedDatoteke = redDatoteke;
                    prviRed = false;
                }
                else brojRedovaPoslijePrvog++;
            }
            obradiPrviRedDatoteke(prviRedDatoteke, brojRedovaPoslijePrvog);
            if (!validnoImportovanjeKviza) {
                zatvoriCitacDatoteke(citac);
                return;
            }
            zatvoriCitacDatoteke(citac);
            citac = dajNoviCitacDatoteke(identifikator);
            if (citac == null) return;
            prviRed = true;
            while ((redDatoteke = citac.readLine()) != null) {
                if (prviRed) {
                    prviRed = false;
                    continue;
                }
                obradiRedDatotekePitanje(redDatoteke);
                if (!validnoImportovanjeKviza) {
                    zatvoriCitacDatoteke(citac);
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            zatvoriCitacDatoteke(citac);
        }
        if (validnoImportovanjeKviza) {
            popuniWidgeteImportovanimKvizom();
            povratniKviz = kvizZaImportovanje;
        }
    }

    private BufferedReader dajNoviCitacDatoteke(Uri identifikator) {
        try {
            return new BufferedReader(new InputStreamReader(getContentResolver().openInputStream(identifikator)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void obradiPrviRedDatoteke(String prviRedDatoteke, int brojRedovaPoslijePrvog) {
        String[] podaciOKvizu = prviRedDatoteke.split(",");

        // Validiranje podataka iz prvog reda datoteke

        int brojUcitanihPodatakaOKvizu = podaciOKvizu.length;
        if (brojUcitanihPodatakaOKvizu != 3) {
            validnoImportovanjeKviza = false;
            prikaziAlertNeispravanFormatDatoteke();
            return;
        }
        boolean kvizVecPostoji = false;
        for (Kviz kviz : listaDosadasnjihKvizova) {
            if (podaciOKvizu[0].equals(kviz.getNaziv())) {
                kvizVecPostoji = true;
                break;
            }
        }
        if (kvizVecPostoji) {
            validnoImportovanjeKviza = false;
            prikaziAlertPostojeciKviz();
            return;
        }
        int ucitaniBrojPitanja = 0;
        try {
            ucitaniBrojPitanja = Integer.parseInt(podaciOKvizu[2].trim());
        } catch (NumberFormatException greskaParsanja) {
            validnoImportovanjeKviza = false;
            prikaziAlertNeispravanFormatDatoteke();
            return;
        }
        if (brojRedovaPoslijePrvog != ucitaniBrojPitanja) {
            validnoImportovanjeKviza = false;
            prikaziAlertNeispravanBrojPitanja();
            return;
        }

        // Ako je sve validno radi dalje

        // Obrada iscitanog naziva kviza

        kvizZaImportovanje.setNaziv(podaciOKvizu[0]);

        // Obrada iscitanog naziva kategorije

        Kategorija novaKategorija = new Kategorija();
        novaKategorija.setNaziv(podaciOKvizu[1]);
        kvizZaImportovanje.setKategorija(novaKategorija);
    }

    private void prikaziAlertNeispravanFormatDatoteke() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Datoteka kviza kojeg importujete nema ispravan format!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void prikaziAlertPostojeciKviz() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Kviz kojeg importujete već postoji!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void prikaziAlertNeispravanBrojPitanja() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Kviz kojeg importujete ima neispravan broj pitanja!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void zatvoriCitacDatoteke(BufferedReader citac) {
        if (citac != null){
            try {
                citac.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void popuniWidgeteImportovanimKvizom() {

        // Postavljanje imena kviza u EditText

        etNaziv.setText(kvizZaImportovanje.getNaziv());

        // Postavljanje kategorije kviza u Spinner

        int pozicijaKategorije = 0;
        boolean postojiKategorija = false;
        for (int i = 0; i < listaKategorija.size(); i++) {
            if (listaKategorija.get(i).getNaziv().equals(kvizZaImportovanje.getKategorija().getNaziv())) {
                postojiKategorija = true;
                pozicijaKategorije = i;
                break;
            }
        }
        if (postojiKategorija) {
            spKategorije.setSelection(pozicijaKategorije);
            pozicijaOdabraneKategorije = pozicijaKategorije;
        }
        else {
            dozvoljeniIdKategorijeImportovanog--;
            while (ikonaZauzeta(dozvoljeniIdKategorijeImportovanog)) dozvoljeniIdKategorijeImportovanog--;
            kvizZaImportovanje.getKategorija().setId(String.valueOf(dozvoljeniIdKategorijeImportovanog));
            dodajNovuKategoriju(kvizZaImportovanje.getKategorija());
            osvjeziPrikazKategorija();
            pozicijaOdabraneKategorije = listaKategorija.size() - 2;
            spKategorije.setSelection(pozicijaOdabraneKategorije);
        }

        // Postavljanje pitanja kviza u ListView

        listaPitanjaUKvizu = kvizZaImportovanje.getPitanja();
        povratniKviz.setPitanja(listaPitanjaUKvizu);
        dodanaPitanjaAdapter = new PitanjaAdapter(this, listaPitanjaUKvizu, getResources());
        lvDodanaPitanja.setAdapter(dodanaPitanjaAdapter);
        listaPitanjaVanKviza = new ArrayList<>();
        mogucaPitanjaAdapter = new PitanjaAdapter(this, listaPitanjaVanKviza, getResources());
        lvMogucaPitanja.setAdapter(mogucaPitanjaAdapter);
    }

    private boolean ikonaZauzeta(int dozvoljeniIdKategorijeImportovanog) {
        for (Kategorija kategorija : listaKategorija) {
            if (kategorija.getId() == null) continue;
            if (kategorija.getId().trim().equals("")) continue;
            if (Integer.parseInt(kategorija.getId()) == dozvoljeniIdKategorijeImportovanog) return true;
        }
        return false;
    }

    private void obradiRedDatotekePitanje(String redDatoteke) {
        String[] podaciOPitanju = redDatoteke.split(",");

        // Validiranje podataka iz reda datoteke

        int ucitaniBrojPodatakaOPitanju = podaciOPitanju.length;
        if (podaciOPitanju.length < 4) {
            validnoImportovanjeKviza = false;
            prikaziAlertNeispravanFormatDatoteke();
            return;
        }
        String ucitaniNazivPitanja = podaciOPitanju[0];
        boolean imaDupliciranNazivPitanja = false;
        for (Pitanje pitanje : kvizZaImportovanje.getPitanja()) {
            if (pitanje.getNaziv().equals(ucitaniNazivPitanja)) {
                imaDupliciranNazivPitanja = true;
                break;
            }
        }
        if (imaDupliciranNazivPitanja) {
            validnoImportovanjeKviza = false;
            prikaziAlertDupliciranoPitanje();
            return;
        }
        int brojOdgovora = podaciOPitanju.length - 3;
        int ucitaniBrojOdgovora = 0;
        try {
            ucitaniBrojOdgovora = Integer.parseInt(podaciOPitanju[1].trim());
        } catch (NumberFormatException greskaParsanja) {
            validnoImportovanjeKviza = false;
            prikaziAlertNeispravanFormatDatoteke();
            return;
        }
        if (brojOdgovora != ucitaniBrojOdgovora) {
            validnoImportovanjeKviza = false;
            prikaziAlertNeispravanBrojOdgovora();
            return;
        }
        int ucitaniIndexTacnogOdgovora = 0;
        try {
            ucitaniIndexTacnogOdgovora = Integer.parseInt(podaciOPitanju[2].trim());
        } catch (NumberFormatException greskaParsanja) {
            validnoImportovanjeKviza = false;
            prikaziAlertNeispravanFormatDatoteke();
            return;
        }
        if ((ucitaniIndexTacnogOdgovora < 0) || (ucitaniIndexTacnogOdgovora >= brojOdgovora)) {
            validnoImportovanjeKviza = false;
            prikaziAlertNeispravanIndexTacnogOdgovora();
            return;
        }
        ArrayList<String> listaUcitanihOdgovora = new ArrayList<>();
        boolean imaDupliciranOdgovor = false;
        vanjskaPetlja: for (int i = 3; i < podaciOPitanju.length; i++) {
            String ucitaniOdgovor = podaciOPitanju[i];
            for (String odgovor : listaUcitanihOdgovora) {
                if (ucitaniOdgovor.equals(odgovor)) {
                    imaDupliciranOdgovor = true;
                    break vanjskaPetlja;
                }
            }
            listaUcitanihOdgovora.add(ucitaniOdgovor);
        }
        if (imaDupliciranOdgovor) {
            validnoImportovanjeKviza = false;
            prikaziAlertDupliciranOdgovor();
            return;
        }

        // Ako je sve validno radi dalje

        // Obrada iscitanog pitanja i njegovih odgovora

        Pitanje novoPitanje = new Pitanje();
        novoPitanje.setNaziv(podaciOPitanju[0]);
        novoPitanje.setTekstPitanja(podaciOPitanju[0]);
        novoPitanje.setOdgovori(listaUcitanihOdgovora);
        novoPitanje.setTacan(podaciOPitanju[ucitaniIndexTacnogOdgovora + 3]);
        kvizZaImportovanje.dodajPitanje(novoPitanje);
    }

    private void prikaziAlertDupliciranoPitanje() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Kviz nije ispravan postoje dva pitanja sa istim nazivom!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void prikaziAlertNeispravanBrojOdgovora() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Kviz kojeg importujete ima neispravan broj odgovora!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void prikaziAlertNeispravanIndexTacnogOdgovora() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Kviz kojeg importujete ima neispravan index tačnog odgovora!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void prikaziAlertDupliciranOdgovor() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Kviz kojeg importujete nije ispravan postoji ponavljanje odgovora!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void postaviListenerNaPostojeceDodijeljenoPitanje() {
        lvDodanaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pitanje pitanjeZaPrebacivanje = listaPitanjaUKvizu.get(position);
                listaPitanjaUKvizu.remove(position);
                listaPitanjaVanKviza.add(pitanjeZaPrebacivanje);
                osvjeziPrikazDodanihPitanja();
                osvjeziPrikazMogucihPitanja();
            }
        });
    }

    private void postaviListenerNaPostojeceMogucePitanje() {
        lvMogucaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pitanje pitanjeZaPrebacivanje = listaPitanjaVanKviza.get(position);
                listaPitanjaVanKviza.remove(position);
                listaPitanjaUKvizu.add(pitanjeZaPrebacivanje);
                osvjeziPrikazDodanihPitanja();
                osvjeziPrikazMogucihPitanja();
            }
        });
    }

    @Override
    public void onBackPressed() {

        // Eventualno dodane kategorije trebaju se prenijeti i u aktivnost KvizoviAkt

        Intent intentPovratakNaKvizove = new Intent();
        intentPovratakNaKvizove.putParcelableArrayListExtra("extraUredjenaListaKategorija", listaKategorija);
        setResult(RESULT_CANCELED, intentPovratakNaKvizove);
        finish();
    }

    @Override
    public void naSaznanjeOPostojanjuKvizaPoNazivu(int resultCode, Bundle resultData) {
        if (resultCode == ProvjeraKvizaPoNazivuService.KVIZ_VEC_POSTOJI) {
            etNaziv.setBackgroundColor(getResources().getColor(R.color.colorLightRed));
            prikaziAlertPostojeciKvizUBazi();
        }
        else if (resultCode == ProvjeraKvizaPoNazivuService.KVIZ_NE_POSTOJI) {
            if (!jeLiPrazanNazivKviza()) resetujPozadinuEtNaziv();
            if (!validnaForma) return;
            povratniKviz.setNaziv(etNaziv.getText().toString());
            povratniKviz.setPitanja(listaPitanjaUKvizu);
            povratniKviz.setKategorija(listaKategorija.get(spKategorije.getSelectedItemPosition()));
            if (dodavanjeNovogKviza) dodajNoviKvizUFirebase(povratniKviz);
            else izmijeniPostojeciKvizPoIdjuUFirebaseu(povratniKviz);
        }
    }

    private void vratiSeUKvizoviAkt() {
        Intent intentPovratakNaKvizove = new Intent();
        if (!getIntent().getBooleanExtra("extraDodavanjeNovogKviza", false)) {
            intentPovratakNaKvizove.putExtra("extraPozicijaPovratnogKviza",
                    getIntent().getIntExtra("extraPozicijaKvizaUListi", 0));
        }
        intentPovratakNaKvizove.putExtra("extraPovratniKviz", (Serializable) povratniKviz);
        intentPovratakNaKvizove.putExtra("extraPozicijaPovratnogKviza", pozicijaKvizaZaUredjivanje);
        intentPovratakNaKvizove.putParcelableArrayListExtra("extraUredjenaListaKategorija", listaKategorija);
        setResult(RESULT_OK, intentPovratakNaKvizove);
        finish();
    }

    private void prikaziAlertPostojeciKvizUBazi() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Uneseni kviz već postoji!");
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void dodajNoviKvizUFirebase(Kviz kviz) {
        DodavanjeKvizaReceiver dodavanjeKvizaReceiver = new DodavanjeKvizaReceiver(new Handler());
        dodavanjeKvizaReceiver.setReceiver(this);
        Intent intent = new Intent(this, DodavanjeKvizaService.class);
        intent.putExtra("extraDodavanjeKvizaReceiver", dodavanjeKvizaReceiver);
        intent.putExtra("extraKvizZaDodavanje", (Serializable) povratniKviz);
        startService(intent);
    }

    private void izmijeniPostojeciKvizPoIdjuUFirebaseu(Kviz kviz) {
        MijenjanjeKvizaPoIdjuReceiver mijenjanjeKvizaPoIdjuReceiver = new MijenjanjeKvizaPoIdjuReceiver(new Handler());
        mijenjanjeKvizaPoIdjuReceiver.setReceiver(this);
        Intent intent = new Intent(this, MijenjanjeKvizaPoIdjuService.class);
        intent.putExtra("extraMijenjanjeKvizaReceiver", mijenjanjeKvizaPoIdjuReceiver);
        intent.putExtra("extraKvizZaMijenjanje", (Serializable) povratniKviz);
        startService(intent);
    }

    @Override
    public void naPrimanjeUcitanihKategorija(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeKategorijaService.KATEGORIJE_UCITANE) {
            if (resultData.containsKey("extraUcitaneKategorije")) {
                Log.d("status", "ima kljuc ucitane kat");
                ArrayList<Kategorija> primljeneKategorije = resultData.getParcelableArrayList("extraUcitaneKategorije");
                if (primljeneKategorije != null) {
                    listaKategorija.addAll(primljeneKategorije);
                    pripremiDodavanjeKategorijeZaSpinner();
                    nastaviSaKupljenjemPodataka();
                }
            }
        }
    }

    private void pripremiDodavanjeKategorijeZaSpinner() {
        if (!jeLiVecInicijalizovanSpinner()) {
            Kategorija kategorijaFiktivna = new Kategorija();
            kategorijaFiktivna.setNaziv(getResources().getString(R.string.spinner_item_dodaj_kategoriju));
            listaKategorija.add(kategorijaFiktivna);
        }
    }

    private boolean jeLiVecInicijalizovanSpinner() {
        String nazivFiktivneKategorije = getResources().getString(R.string.spinner_item_dodaj_kategoriju);
        for (Kategorija kategorija : listaKategorija) {
            if (kategorija.getNaziv().equals(nazivFiktivneKategorije)) {
                return true;
            }
        }
        return false;
    }

    private void nastaviSaKupljenjemPodataka() {
        if (getIntent().getBooleanExtra("extraDodavanjeNovogKviza", false)) {
            povratniKviz = new Kviz("", new ArrayList<Pitanje>(), null);
            dodavanjeNovogKviza = true;
            listaDosadasnjihKvizova = getIntent().getParcelableArrayListExtra("extraListaDosadasnjihKvizova");
            ucitajSvaPitanjaUMoguca();
        }
        else {
            povratniKviz = (Kviz) getIntent().getSerializableExtra("extraKvizZaUredjivanje");
            pozicijaKvizaZaUredjivanje = getIntent().getIntExtra("extraPozicijaKvizaUListi", 0);
            listaPitanjaUKvizu = povratniKviz.getPitanja();
            String idDokumentaKviza = povratniKviz.getIdIzFirebasea();
            Log.d("IDDOKKviz", idDokumentaKviza);
            dodavanjeNovogKviza = false;
            ucitajTrenutniKvizPoIdjuIzFirebaseaIOsvjeziEkran(idDokumentaKviza);
        }
    }

    private void ucitajSvaPitanjaUMoguca() {
        UcitavanjePitanjaReceiver ucitavanjePitanjaReceiver = new UcitavanjePitanjaReceiver(new Handler());
        ucitavanjePitanjaReceiver.setReceiver(this);
        Intent intent = new Intent(this, UcitavanjePitanjaService.class);
        intent.putExtra("extraUcitavanjePitanjaReceiver", ucitavanjePitanjaReceiver);
        startService(intent);
    }

    private void postaviOstaloNakonUcitavanja() {
        onemoguciImportAkoJeMijenjanje();
        postaviAdaptere();
        popuniWidgetePostojecimPodacima();
        inicijalizirajDozvoljeniIdKategorijeImportovanog();
        postaviListenerZaSkrolanjeListeDodanihPitanja();
        postaviListenerZaSkrolanjeListeMogucihPitanja();
        postaviListenerNaKategoriju();
        postaviListenerNaDugmeImportovanjeKviza();
        postaviListenerNaDugmeDodavanjeKviza();
        postaviListenerNaFooterDodavanjePitanja();
        postaviListenerNaPostojeceDodijeljenoPitanje();
        postaviListenerNaPostojeceMogucePitanje();
    }

    private void ucitajTrenutniKvizPoIdjuIzFirebaseaIOsvjeziEkran(String idDokumenta) {
        UcitavanjeKvizaPoIdjuReceiver ucitavanjeKvizaPoIdjuReceiver = new UcitavanjeKvizaPoIdjuReceiver(new Handler());
        ucitavanjeKvizaPoIdjuReceiver.setReceiver(this);
        Intent intent = new Intent(this, UcitavanjeKvizaPoIdjuService.class);
        intent.putExtra("extraUcitavanjeKvizaPoIdjuReceiver", ucitavanjeKvizaPoIdjuReceiver);
        intent.putExtra("extraZadaniIdDokumentaKviza", idDokumenta);
        startService(intent);
    }

    @Override
    public void naPrimanjeUcitanogKvizaPoIdju(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeKvizaPoIdjuService.KVIZ_UCITAN) {
            if (resultData.containsKey("extraUcitaniKvizPoIdju")) {
                Kviz primljeniKviz = (Kviz) resultData.getSerializable("extraUcitaniKvizPoIdju");
                ArrayList<Pitanje> primljenaMogucaPitanja = resultData.getParcelableArrayList("extraOstalaPitanja");
                if (primljenaMogucaPitanja != null) listaPitanjaVanKviza = primljenaMogucaPitanja;
                if (primljeniKviz != null) {
                    povratniKviz = primljeniKviz;
                    prijasnjiNazivKviza = povratniKviz.getNaziv();
                    listaDosadasnjihKvizova = getIntent().getParcelableArrayListExtra("extraListaDosadasnjihKvizova");
                    postaviOstaloNakonUcitavanja();
                }
            }
        }
    }

    @Override
    public void naSaznanjeODodanomKvizu(int resultCode, Bundle resultData) {
        if (resultCode == DodavanjeKvizaService.KVIZ_DODAN) {
            vratiSeUKvizoviAkt();
        }
    }

    @Override
    public void naSaznanjeOIzmijenjenomKvizu(int resultCode, Bundle resultData) {
        if (resultCode == MijenjanjeKvizaPoIdjuService.KVIZ_IZMIJENJEN) {
            if (!prijasnjiNazivKviza.equals(etNaziv.getText().toString())) ucitajRangListuIzFirebasea();
            else vratiSeUKvizoviAkt();
        }
    }

    private void ucitajRangListuIzFirebasea() {
        UcitavanjeRanglistePoNazivuKvizaReceiver ucitavanjeRanglistePoNazivuKvizaReceiver = new UcitavanjeRanglistePoNazivuKvizaReceiver(new Handler());
        ucitavanjeRanglistePoNazivuKvizaReceiver.setReceiver(this);
        Intent intent = new Intent(this, UcitavanjeRanglistePoNazivuKvizaService.class);
        intent.putExtra("extraUcitavanjeRangListePoNazivuKvizaReceiver", ucitavanjeRanglistePoNazivuKvizaReceiver);
        intent.putExtra("extraNazivZadanogKviza", prijasnjiNazivKviza);
        startService(intent);
    }

    @Override
    public void naPrimanjeUcitanihPitanja(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjePitanjaService.PITANJA_UCITANA) {
            if (resultData.containsKey("extraUcitanaPitanja")) {
                ArrayList<Pitanje> primljenaPitanja = resultData.getParcelableArrayList("extraUcitanaPitanja");
                if (primljenaPitanja != null) {
                    listaPitanjaVanKviza = new ArrayList<>();
                    listaPitanjaVanKviza.addAll(primljenaPitanja);
                    postaviOstaloNakonUcitavanja();
                }
            }
        }
    }

    @Override
    public void naPrimanjeUcitaneRangListePoNazivuKviza(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeRanglistePoNazivuKvizaService.NEMA_RANGLISTE_U_BAZI) {
            vratiSeUKvizoviAkt();
        }
        else if (resultCode == UcitavanjeRanglistePoNazivuKvizaService.RANGLISTA_UCITANA_PO_NAZIVU_KVIZA) {
            RangListaObjekat primljenaLista = (RangListaObjekat) resultData.getSerializable("extraUcitanaRangListaPoKvizu");
            if (primljenaLista != null) rangListaKviza = primljenaLista;
            rangListaKviza.setNazivKviza(etNaziv.getText().toString());
            izmijeniPostojecuRangListuUFirebaseu();
            vratiSeUKvizoviAkt();
        }
    }

    private void izmijeniPostojecuRangListuUFirebaseu() {
        Intent intent = new Intent(this, DodavanjeElementaURanglistuService.class);
        intent.putExtra("extraRangListaZaMijenjanje", (Serializable) rangListaKviza);
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prvo = true;
        internetBroadcastReceiver.setAktivnost(this);
        registerReceiver(internetBroadcastReceiver, internetIntentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(internetBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void izmijeniStatusKonekcije(boolean imaKonekcije) {
        imaInternetKonekcije = imaKonekcije;
        if (imaKonekcije) omoguciDodavanjeIImportovanje();
        else onemoguciDodavanjeIImportovanje();
        if (prvo) {
            prvo = false;
            return;
        }
        if (imaKonekcije) new OsvjezivacPodatakaUBazama(this).azurirajStanjeUSQLiteuIFirebaseu();
    }

    private void omoguciDodavanjeIImportovanje() {
        if (!btnDodajKviz.isEnabled()) btnDodajKviz.setEnabled(true);
        if (getIntent().getBooleanExtra("extraDodavanjeNovogKviza", false)) {
            if (!btnImportKviz.isEnabled()) btnImportKviz.setEnabled(true);
        }
    }

    private void onemoguciDodavanjeIImportovanje() {
        if (btnDodajKviz.isEnabled()) btnDodajKviz.setEnabled(false);
        if (getIntent().getBooleanExtra("extraDodavanjeNovogKviza", false)) {
            if (btnImportKviz.isEnabled()) btnImportKviz.setEnabled(false);
        }
    }
}
