package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodavanjePitanjaService extends IntentService {

    public static final int PITANJE_DODANO = 0;
    private Pitanje pitanjeZaDodavanje = new Pitanje();

    public DodavanjePitanjaService() {
        super(null);
    }

    public DodavanjePitanjaService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("stat", "dodavanjePitanjaPokrenuto");
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraDodavanjePitanjaReceiver");
        pitanjeZaDodavanje = (Pitanje) intent.getSerializableExtra("extraPitanjeZaDodavanje");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Pitanja?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            JSONObject pitanjeObject = new JSONObject();
            JSONObject poljaObject = new JSONObject();
            JSONObject nazivObject = new JSONObject();
            JSONObject indexTacnogObject = new JSONObject();
            JSONObject odgovoriObject = new JSONObject();
            JSONObject arrayValueObject = new JSONObject();
            JSONArray vrijednostiArray = new JSONArray();
            ArrayList<String> odgovoriPitanja = pitanjeZaDodavanje.getOdgovori();
            int indexTacnog = 0;
            for (int i = 0; i < odgovoriPitanja.size(); i++) {
                if (odgovoriPitanja.get(i).equals(pitanjeZaDodavanje.getTacan())) {
                    indexTacnog = i;
                    break;
                }
            }
            try {
                for (int i = 0; i < odgovoriPitanja.size(); i++) {
                    JSONObject pojedinacniOdgovorObject = new JSONObject();
                    pojedinacniOdgovorObject.put("stringValue", odgovoriPitanja.get(i));
                    vrijednostiArray.put(i, pojedinacniOdgovorObject);
                }
                arrayValueObject.put("values", vrijednostiArray);
                nazivObject.put("stringValue", pitanjeZaDodavanje.getNaziv());
                indexTacnogObject.put("integerValue", String.valueOf(indexTacnog));
                odgovoriObject.put("arrayValue", arrayValueObject);
                poljaObject.put("naziv", nazivObject);
                poljaObject.put("indexTacnog", indexTacnogObject);
                poljaObject.put("odgovori", odgovoriObject);
                pitanjeObject.put("fields", poljaObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String dokument = pitanjeObject.toString();
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = dokument.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                String dobijenoNazad = response.toString();
                Log.d("ODGOVOR", response.toString());
                JSONObject dobijeniNazadObject = new JSONObject(dobijenoNazad);
                String spojeniNizString = dobijeniNazadObject.getString("name");
                String[] nizString = spojeniNizString.split("/");
                int duzinaNizStringa = nizString.length;
                String idDokumentaPitanjaString = nizString[duzinaNizStringa - 1];
                bundle.putString("extraVraceniIdPitanja", idDokumentaPitanjaString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            resultReceiver.send(PITANJE_DODANO, bundle);
            Log.d("status", "zavrsilo dodavanje pitanja");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
