package ba.unsa.etf.rma.resultreceiveri;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class DodavanjePitanjaReceiver extends ResultReceiver {

    private Receiver receiver;

    public DodavanjePitanjaReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public interface Receiver {
        void naSaznanjeODodanomPitanju(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (receiver != null) receiver.naSaznanjeODodanomPitanju(resultCode, resultData);
    }
}
