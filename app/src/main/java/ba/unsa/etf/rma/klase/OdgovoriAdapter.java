package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class OdgovoriAdapter extends BaseAdapter {

    private Activity aktivnost;
    private ArrayList<String> listaOdgovora;
    private static LayoutInflater inflater = null;
    private Resources resursi;
    private String odgovor = null;
    private String tacanOdgovor = "";

    public OdgovoriAdapter(Activity aktivnost, ArrayList<String> listaOdgovora, Resources resursi) {
        this.aktivnost = aktivnost;
        this.listaOdgovora = listaOdgovora;
        this.resursi = resursi;
        inflater = (LayoutInflater) aktivnost.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (listaOdgovora.size() == 0) return 1;
        return listaOdgovora.size();
    }

    @Override
    public Object getItem(int position) {
        return listaOdgovora.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setTacanOdgovor(String tacanOdgovor) {
        this.tacanOdgovor = tacanOdgovor;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View noviView = convertView;
        PomocniOdgovor pomocniOdgovor;
        if (convertView == null) {
            noviView = inflater.inflate(R.layout.element_liste_odgovora, null);
            pomocniOdgovor = new PomocniOdgovor();
            pomocniOdgovor.tekstOdgovora = (TextView) noviView.findViewById(R.id.tvOdgovor);
            noviView.setTag(pomocniOdgovor);
        }
        else
            pomocniOdgovor = (PomocniOdgovor) noviView.getTag();
        if (listaOdgovora.size() == 0) {
            pomocniOdgovor.tekstOdgovora.setText(" ");
            postaviPozadinuElementa(noviView, Color.TRANSPARENT);
        }
        else {
            odgovor = listaOdgovora.get(position);
            pomocniOdgovor.tekstOdgovora.setText(odgovor);
            if (odgovor.equals(tacanOdgovor)) {
                postaviPozadinuElementa(noviView, resursi.getColor(R.color.colorLightGreen));
            }
            else {
                postaviPozadinuElementa(noviView, Color.TRANSPARENT);
            }
        }
        return noviView;
    }

    public static class PomocniOdgovor {
        public TextView tekstOdgovora;
    }

    private void postaviPozadinuElementa(View view, int boja) {
        view.setBackgroundColor(boja);
    }
}
