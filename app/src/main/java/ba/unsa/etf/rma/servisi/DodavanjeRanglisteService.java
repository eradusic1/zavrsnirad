package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.ElementRangListe;
import ba.unsa.etf.rma.klase.RangListaObjekat;

public class DodavanjeRanglisteService extends IntentService {

    public static final int RANGLISTA_DODANA = 0;
    private RangListaObjekat rangListaZaDodavanje = new RangListaObjekat();

    public DodavanjeRanglisteService() {
        super(null);
    }

    public DodavanjeRanglisteService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("stat", "dodavanjeListePokrenuto");
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraDodavanjeRanglisteReceiver");
        rangListaZaDodavanje = (RangListaObjekat) intent.getSerializableExtra("extraRangListaZaDodavanje");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents/Rangliste?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            JSONObject rangListaObject = new JSONObject();
            JSONObject poljaObject = new JSONObject();
            JSONObject nazivKvizaObject = new JSONObject();
            JSONObject listaObject = new JSONObject();
            JSONObject listaMapValue = new JSONObject();
            JSONObject poljaMapeObject = new JSONObject();
            try {
                nazivKvizaObject.put("stringValue", rangListaZaDodavanje.getNazivKviza());
                poljaObject.put("nazivKviza", nazivKvizaObject);
                ArrayList<ElementRangListe> elementiRangListe = rangListaZaDodavanje.getLista();
                for (int i = 0; i < elementiRangListe.size(); i++) {
                    JSONObject procenatObject = new JSONObject();
                    procenatObject.put("stringValue", elementiRangListe.get(i).getProcenatTacnosti());
                    JSONObject unutarnjaPoljaMapeObject = new JSONObject();
                    unutarnjaPoljaMapeObject.put(elementiRangListe.get(i).getImeIgraca(), procenatObject);
                    JSONObject unutarnjaMapaObject = new JSONObject();
                    unutarnjaMapaObject.put("fields", unutarnjaPoljaMapeObject);
                    JSONObject redniBrojObject = new JSONObject();
                    redniBrojObject.put("mapValue", unutarnjaMapaObject);
                    poljaMapeObject.put(String.valueOf(i + 1), redniBrojObject);
                }
                listaMapValue.put("fields", poljaMapeObject);
                listaObject.put("mapValue", listaMapValue);
                poljaObject.put("lista", listaObject);
                rangListaObject.put("fields", poljaObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String dokument = rangListaObject.toString();
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = dokument.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                Log.d("ODGOVOR", response.toString());
            }
            Log.d("status", "zavrsilo dodavanje rangliste");
            if (resultReceiver != null) resultReceiver.send(RANGLISTA_DODANA, bundle);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
