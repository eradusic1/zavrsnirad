package ba.unsa.etf.rma.korisneklase;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;

import java.io.Serializable;
import java.util.ArrayList;

import ba.unsa.etf.rma.klase.ElementRangListe;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.RangListaObjekat;
import ba.unsa.etf.rma.resultreceiveri.DodavanjeElementaURanglistuReceiver;
import ba.unsa.etf.rma.resultreceiveri.DodavanjeRanglisteReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeKategorijaReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeKvizovaReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjePitanjaReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeRangListiReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeRanglistePoNazivuKvizaReceiver;
import ba.unsa.etf.rma.servisi.DodavanjeElementaURanglistuService;
import ba.unsa.etf.rma.servisi.DodavanjeRanglisteService;
import ba.unsa.etf.rma.servisi.UcitavanjeKategorijaService;
import ba.unsa.etf.rma.servisi.UcitavanjeKvizovaService;
import ba.unsa.etf.rma.servisi.UcitavanjePitanjaService;
import ba.unsa.etf.rma.servisi.UcitavanjeRanglistePoNazivuKvizaService;
import ba.unsa.etf.rma.servisi.UcitavanjeRanglistiService;
import ba.unsa.etf.rma.sqliteopenhelperi.BazaSQLiteOpenHelper;

public class OsvjezivacPodatakaUBazama implements UcitavanjeKategorijaReceiver.Receiver,
        UcitavanjePitanjaReceiver.Receiver, UcitavanjeKvizovaReceiver.Receiver,
        UcitavanjeRangListiReceiver.Receiver, UcitavanjeRanglistePoNazivuKvizaReceiver.Receiver,
        DodavanjeRanglisteReceiver.Receiver, DodavanjeElementaURanglistuReceiver.Receiver {

    private Context context;
    private BazaSQLiteOpenHelper bazaSQLiteOpenHelper;
    private ArrayList<Kategorija> ucitaneKategorijeIzFirebasea = new ArrayList<>();
    private ArrayList<Pitanje> ucitanaPitanjaIzFirebasea = new ArrayList<>();
    private ArrayList<Kviz> ucitaniKvizoviIzFirebasea = new ArrayList<>();
    private ArrayList<RangListaObjekat> ucitaneRangListeIzFirebasea = new ArrayList<>();
    private RangListaObjekat trenutnaRangLista = new RangListaObjekat();
    private String trenutniNazivKviza = "";
    private ArrayList<RangListaObjekat> rangListeZaDodati = new ArrayList<>();
    private int trenutniIndex = 0;
    private ProgressDialog progressDialog;

    public OsvjezivacPodatakaUBazama(Context context) {
        this.context = context;
        bazaSQLiteOpenHelper = new BazaSQLiteOpenHelper(context, BazaSQLiteOpenHelper.NAZIV_BAZE,
                null, BazaSQLiteOpenHelper.VERZIJA_BAZE);
        progressDialog = new ProgressDialog(context);
    }

    public void azurirajStanjeUSQLiteuIFirebaseu() {
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage("Osvjezavanje lokalne baze...");
            progressDialog.show();
        }
        obrisiSveIzSQLitea();
        ucitajSveKategorijeIzFirebasea();
    }

    private void obrisiSveIzSQLitea() {
        SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
        db.delete(BazaSQLiteOpenHelper.NAZIV_TABELE_KVIZOVI, null, null);
        db.delete(BazaSQLiteOpenHelper.NAZIV_TABELE_KATEGORIJE, null, null);
        db.delete(BazaSQLiteOpenHelper.NAZIV_TABELE_PITANJA, null, null);
        db.delete(BazaSQLiteOpenHelper.NAZIV_TABELE_ODGOVORI, null, null);
        db.delete(BazaSQLiteOpenHelper.NAZIV_TABELE_PITANJE_U_KVIZU, null, null);
        db.close();
    }

    private void ucitajSveKategorijeIzFirebasea() {
        UcitavanjeKategorijaReceiver ucitavanjeKategorijaReceiver = new UcitavanjeKategorijaReceiver(new Handler());
        ucitavanjeKategorijaReceiver.setReceiver(this);
        Intent intent = new Intent(context, UcitavanjeKategorijaService.class);
        intent.putExtra("extraUcitavanjeKategorijaReceiver", ucitavanjeKategorijaReceiver);
        context.startService(intent);
    }

    @Override
    public void naPrimanjeUcitanihKategorija(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeKategorijaService.KATEGORIJE_UCITANE) {
            if (resultData.containsKey("extraUcitaneKategorije")) {
                ArrayList<Kategorija> primljeneKategorije = resultData.getParcelableArrayList("extraUcitaneKategorije");
                if (primljeneKategorije != null) {
                    ucitaneKategorijeIzFirebasea = new ArrayList<>();
                    ucitaneKategorijeIzFirebasea.addAll(primljeneKategorije);
                    citajDaljeNakonKategorija();
                }
            }
        }
    }

    private void citajDaljeNakonKategorija() {
        ucitajSvaPitanjaIzFirestorea();
    }

    private void ucitajSvaPitanjaIzFirestorea() {
        UcitavanjePitanjaReceiver ucitavanjePitanjaReceiver = new UcitavanjePitanjaReceiver(new Handler());
        ucitavanjePitanjaReceiver.setReceiver(this);
        Intent intent = new Intent(context, UcitavanjePitanjaService.class);
        intent.putExtra("extraUcitavanjePitanjaReceiver", ucitavanjePitanjaReceiver);
        context.startService(intent);
    }

    @Override
    public void naPrimanjeUcitanihPitanja(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjePitanjaService.PITANJA_UCITANA) {
            if (resultData.containsKey("extraUcitanaPitanja")) {
                ArrayList<Pitanje> primljenaPitanja = resultData.getParcelableArrayList("extraUcitanaPitanja");
                if (primljenaPitanja != null) {
                    ucitanaPitanjaIzFirebasea = new ArrayList<>();
                    ucitanaPitanjaIzFirebasea.addAll(primljenaPitanja);
                    citajDaljeNakonPitanja();
                }
            }
        }
    }

    private void citajDaljeNakonPitanja() {
        ucitajSveKvizoveIzFirebasea();
    }

    private void ucitajSveKvizoveIzFirebasea() {
        UcitavanjeKvizovaReceiver ucitavanjeKvizovaReceiver = new UcitavanjeKvizovaReceiver(new Handler());
        ucitavanjeKvizovaReceiver.setReceiver(this);
        Intent intent = new Intent(context, UcitavanjeKvizovaService.class);
        intent.putExtra("extraUcitavanjeKvizovaReceiver", ucitavanjeKvizovaReceiver);
        context.startService(intent);
    }

    @Override
    public void naPrimanjeUcitanihKvizova(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeKvizovaService.KVIZOVI_UCITANI) {
            if (resultData.containsKey("extraUcitaniKvizovi")) {
                ArrayList<Kviz> primljeniKvizovi = resultData.getParcelableArrayList("extraUcitaniKvizovi");
                if (primljeniKvizovi != null) {
                    ucitaniKvizoviIzFirebasea = new ArrayList<>();
                    ucitaniKvizoviIzFirebasea.addAll(primljeniKvizovi);
                    pocniSaUpisivanjemSvegaUSQLite();
                }
            }
        }
    }

    private void pocniSaUpisivanjemSvegaUSQLite() {
        upisiKategorijeUSQLite();
        upisiPitanjaUSQLite();
        upisiKvizoveUSQLite();
        upisiNoveRangListeUFirebase();
    }

    private void upisiKategorijeUSQLite() {
        for (Kategorija kategorija : ucitaneKategorijeIzFirebasea) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(BazaSQLiteOpenHelper.KATEGORIJA_NAZIV, kategorija.getNaziv());
            contentValues.put(BazaSQLiteOpenHelper.KATEGORIJA_ID_IKONICE, kategorija.getId());
            SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
            db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_KATEGORIJE, null, contentValues);
            db.close();
        }
    }

    private void upisiPitanjaUSQLite() {
        for (Pitanje pitanje : ucitanaPitanjaIzFirebasea) {
            upisiOdgovorePitanjaUSQLite(pitanje);
            ContentValues contentValues = new ContentValues();
            contentValues.put(BazaSQLiteOpenHelper.PITANJE_NAZIV, pitanje.getNaziv());
            contentValues.put(BazaSQLiteOpenHelper.PITANJE_TACAN, pitanje.getTacan());
            SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
            db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_PITANJA, null, contentValues);
            db.close();
        }
    }

    private void upisiOdgovorePitanjaUSQLite(Pitanje pitanje) {
        ArrayList<String> odgovori = pitanje.getOdgovori();
        for (String odgovor : odgovori) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(BazaSQLiteOpenHelper.ODGOVOR_TEKST, odgovor);
            contentValues.put(BazaSQLiteOpenHelper.ODGOVOR_PITANJE, pitanje.getNaziv());
            SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
            db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_ODGOVORI, null, contentValues);
            db.close();
        }
    }

    private void upisiKvizoveUSQLite() {
        for (Kviz kviz : ucitaniKvizoviIzFirebasea) {
            upisiVezeKvizaINjegovihPitanjaUSQlite(kviz);
            ContentValues contentValues = new ContentValues();
            contentValues.put(BazaSQLiteOpenHelper.KVIZ_NAZIV, kviz.getNaziv());
            contentValues.put(BazaSQLiteOpenHelper.KVIZ_KATEGORIJA, kviz.getKategorija().getNaziv());
            SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
            db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_KVIZOVI, null, contentValues);
            db.close();
        }
    }

    private void upisiVezeKvizaINjegovihPitanjaUSQlite(Kviz kviz) {
        ArrayList<Pitanje> pitanja = kviz.getPitanja();
        for (Pitanje pitanje : pitanja) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(BazaSQLiteOpenHelper.PITANJE_U_KVIZU_KVIZ, kviz.getNaziv());
            contentValues.put(BazaSQLiteOpenHelper.PITANJE_U_KVIZU_PITANJE, pitanje.getNaziv());
            SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
            db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_PITANJE_U_KVIZU, null, contentValues);
            db.close();
        }
    }

    private void upisiNoveRangListeUFirebase() {
        rangListeZaDodati = MjestoZaCuvanjeNovihRangListi.getListaRangListi();
        if (rangListeZaDodati.size() == 0) {
            ucitajSveRangListeIzFirebasea();
            return;
        }
        trenutniIndex = 0;
        trenutniNazivKviza = rangListeZaDodati.get(trenutniIndex).getNazivKviza();
        ucitajRangListuIzFirebaseaPoNazivuKviza(trenutniNazivKviza);
    }

    private void ucitajSveRangListeIzFirebasea() {
        UcitavanjeRangListiReceiver ucitavanjeRangListiReceiver = new UcitavanjeRangListiReceiver(new Handler());
        ucitavanjeRangListiReceiver.setReceiver(this);
        Intent intent = new Intent(context, UcitavanjeRanglistiService.class);
        intent.putExtra("extraUcitavanjeRangListiReceiver", ucitavanjeRangListiReceiver);
        context.startService(intent);
    }

    @Override
    public void naPrimanjeUcitanihRangListi(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeRanglistiService.RANGLISTE_UCITANE) {
            if (resultData.containsKey("extraUcitaneRangListe")) {
                ArrayList<RangListaObjekat> primljeneRangListe = resultData.getParcelableArrayList("extraUcitaneRangListe");
                if (primljeneRangListe != null) {
                    ucitaneRangListeIzFirebasea = new ArrayList<>();
                    ucitaneRangListeIzFirebasea.addAll(primljeneRangListe);
                    osvjeziRangListeLokalno();
                }
            }
        }
    }

    private void ucitajRangListuIzFirebaseaPoNazivuKviza(String nazivKviza) {
        UcitavanjeRanglistePoNazivuKvizaReceiver ucitavanjeRanglistePoNazivuKvizaReceiver = new UcitavanjeRanglistePoNazivuKvizaReceiver(new Handler());
        ucitavanjeRanglistePoNazivuKvizaReceiver.setReceiver(this);
        Intent intent = new Intent(context, UcitavanjeRanglistePoNazivuKvizaService.class);
        intent.putExtra("extraUcitavanjeRangListePoNazivuKvizaReceiver", ucitavanjeRanglistePoNazivuKvizaReceiver);
        intent.putExtra("extraNazivZadanogKviza", nazivKviza);
        context.startService(intent);
    }

    @Override
    public void naPrimanjeUcitaneRangListePoNazivuKviza(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeRanglistePoNazivuKvizaService.NEMA_RANGLISTE_U_BAZI) {
            trenutnaRangLista = nadjiRangListuPoNazivuKviza(trenutniNazivKviza);
            trenutnaRangLista.sortirajListu();
            dodajNovuRangListuUFirebase();
        }
        else if (resultCode == UcitavanjeRanglistePoNazivuKvizaService.RANGLISTA_UCITANA_PO_NAZIVU_KVIZA) {
            RangListaObjekat primljenaLista = (RangListaObjekat) resultData.getSerializable("extraUcitanaRangListaPoKvizu");
            if (primljenaLista != null) trenutnaRangLista = primljenaLista;
            RangListaObjekat nadjenaRangLista = nadjiRangListuPoNazivuKviza(trenutniNazivKviza);
            for (ElementRangListe elementRangListe : nadjenaRangLista.getLista()) {
                trenutnaRangLista.dodajNoviElementUListu(elementRangListe);
            }
            trenutnaRangLista.sortirajListu();
            izmijeniPostojecuRangListuUFirebaseu();
        }
    }

    private void idiDalje() {
        if (trenutniIndex != rangListeZaDodati.size() - 1) {
            trenutniIndex++;
            radiSaSljedecomRangListom();
        }
        else {
            MjestoZaCuvanjeNovihRangListi.resetujListuRangListi();
            rangListeZaDodati = new ArrayList<>();
            ucitajSveRangListeIzFirebasea();
        }
    }

    private void osvjeziRangListeLokalno() {
        SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
        db.delete(BazaSQLiteOpenHelper.NAZIV_TABELE_ELEMENTI_RANGLISTE, null, null);
        db.delete(BazaSQLiteOpenHelper.NAZIV_TABELE_RANGLISTE, null, null);
        db.close();
        upisiRangListeUSQLite();
    }

    private void upisiRangListeUSQLite() {
        for (RangListaObjekat rangListaObjekat : ucitaneRangListeIzFirebasea) {
            upisiElementeRangListeUSQLite(rangListaObjekat);
            ContentValues contentValues = new ContentValues();
            contentValues.put(BazaSQLiteOpenHelper.RANGLISTA_NAZIV_KVIZA, rangListaObjekat.getNazivKviza());
            SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
            db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_RANGLISTE, null, contentValues);
            db.close();
        }
        if (progressDialog.isShowing()) progressDialog.dismiss();
    }

    private void upisiElementeRangListeUSQLite(RangListaObjekat rangListaObjekat) {
        ArrayList<ElementRangListe> elementiRangListe = rangListaObjekat.getLista();
        for (ElementRangListe elementRangListe : elementiRangListe) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_KVIZ, rangListaObjekat.getNazivKviza());
            contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_IME_IGRACA, elementRangListe.getImeIgraca());
            contentValues.put(BazaSQLiteOpenHelper.ELEMENT_RANGLISTE_PROCENAT, elementRangListe.getProcenatTacnosti());
            SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
            db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_ELEMENTI_RANGLISTE, null, contentValues);
            db.close();
        }
    }

    public RangListaObjekat nadjiRangListuPoNazivuKviza(String nazivKviza) {
        for (RangListaObjekat rangListaObjekat : rangListeZaDodati) {
            if (rangListaObjekat.getNazivKviza().equals(nazivKviza)) return rangListaObjekat;
        }
        return null;
    }

    private void dodajNovuRangListuUFirebase() {
        DodavanjeRanglisteReceiver dodavanjeRanglisteReceiver = new DodavanjeRanglisteReceiver(new Handler());
        dodavanjeRanglisteReceiver.setReceiver(this);
        Intent intent = new Intent(context, DodavanjeRanglisteService.class);
        intent.putExtra("extraDodavanjeRanglisteReceiver", dodavanjeRanglisteReceiver);
        intent.putExtra("extraRangListaZaDodavanje", (Serializable) trenutnaRangLista);
        context.startService(intent);
    }

    @Override
    public void naSaznanjeODodanojRangListi(int resultCode, Bundle resultData) {
        if (resultCode == DodavanjeRanglisteService.RANGLISTA_DODANA) idiDalje();
    }

    private void izmijeniPostojecuRangListuUFirebaseu() {
        DodavanjeElementaURanglistuReceiver dodavanjeElementaURanglistuReceiver =
                new DodavanjeElementaURanglistuReceiver(new Handler());
        dodavanjeElementaURanglistuReceiver.setReceiver(this);
        Intent intent = new Intent(context, DodavanjeElementaURanglistuService.class);
        intent.putExtra("extraDodavanjeElementaURanglistuReceiver", dodavanjeElementaURanglistuReceiver);
        intent.putExtra("extraRangListaZaMijenjanje", (Serializable) trenutnaRangLista);
        context.startService(intent);
    }

    @Override
    public void naSaznanjeOIzmijenjenojRangListi(int resultCode, Bundle resultData) {
        if (resultCode == DodavanjeElementaURanglistuService.RANGLISTA_IZMIJENJENA) idiDalje();
    }

    private void radiSaSljedecomRangListom() {
        trenutniNazivKviza = rangListeZaDodati.get(trenutniIndex).getNazivKviza();
        ucitajRangListuIzFirebaseaPoNazivuKviza(trenutniNazivKviza);
    }

    public void upisiNovuKategorijuLokalno(Kategorija kategorija) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BazaSQLiteOpenHelper.KATEGORIJA_NAZIV, kategorija.getNaziv());
        contentValues.put(BazaSQLiteOpenHelper.KATEGORIJA_ID_IKONICE, kategorija.getId());
        SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
        db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_KATEGORIJE, null, contentValues);
        db.close();
    }

    public void upisiNovoPitanjeLokalno(Pitanje pitanje) {
        upisiOdgovorePitanjaUSQLite(pitanje);
        ContentValues contentValues = new ContentValues();
        contentValues.put(BazaSQLiteOpenHelper.PITANJE_NAZIV, pitanje.getNaziv());
        contentValues.put(BazaSQLiteOpenHelper.PITANJE_TACAN, pitanje.getTacan());
        SQLiteDatabase db = bazaSQLiteOpenHelper.getWritableDatabase();
        db.insert(BazaSQLiteOpenHelper.NAZIV_TABELE_PITANJA, null, contentValues);
        db.close();
    }
}
