package ba.unsa.etf.rma.resultreceiveri;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class UcitavanjeKvizaPoIdjuReceiver extends ResultReceiver {

    private Receiver receiver;

    public UcitavanjeKvizaPoIdjuReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public interface Receiver {
        void naPrimanjeUcitanogKvizaPoIdju(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (receiver != null) receiver.naPrimanjeUcitanogKvizaPoIdju(resultCode, resultData);
    }
}
