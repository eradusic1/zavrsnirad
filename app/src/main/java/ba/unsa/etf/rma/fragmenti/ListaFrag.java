package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.KategorijeAdapter;

public class ListaFrag extends Fragment {

    private OnItemClick onItemClick;
    private ArrayList<Kategorija> listaPostojecihKategorija = new ArrayList<>();
    private ListView listaKategorija;
    private KategorijeAdapter kategorijeAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lista_frag, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dobaviWidgete();
        if (getArguments().containsKey("bundleListaKategorija")) {
            listaPostojecihKategorija = getArguments().getParcelableArrayList("bundleListaKategorija");
            kategorijeAdapter = new KategorijeAdapter(getActivity(), listaPostojecihKategorija, getActivity().getResources());
            listaKategorija.setAdapter(kategorijeAdapter);
            try {
                onItemClick = (OnItemClick) getActivity();
            } catch (ClassCastException exception) {
                throw new ClassCastException("Problem sa interfejsom - treba ga implementirati!");
            }
            listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    onItemClick.onKategorijaClicked(position);
                }
            });
        }
    }

    private void dobaviWidgete() {
        listaKategorija = getView().findViewById(R.id.listaKategorija);
    }

    public interface OnItemClick {
        void onKategorijaClicked(int position);
    }
}
