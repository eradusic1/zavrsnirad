package ba.unsa.etf.rma.servisi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import ba.unsa.etf.rma.R;

public class ProvjeraPitanjaPoNazivuService extends IntentService {

    public static final int PITANJE_VEC_POSTOJI = 0;
    public static final int PITANJE_NE_POSTOJI = 1;
    private String nazivZadanogPitanja = "";

    public ProvjeraPitanjaPoNazivuService() {
        super(null);
    }

    public ProvjeraPitanjaPoNazivuService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final ResultReceiver resultReceiver = intent.getParcelableExtra("extraProvjeraPitPoNazivuReceiver");
        nazivZadanogPitanja = intent.getStringExtra("extraNazivZadanogPitanja");
        Bundle bundle = new Bundle();
        GoogleCredential credentials;
        try {
            InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
            credentials = GoogleCredential.fromStream(tajnaStream).
                    createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKK", TOKEN);

            String url = "https://firestore.googleapis.com/v1/projects/dbrdarspirala3/databases/(default)/documents:runQuery?access_token=";
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            JSONObject jsonObject = new JSONObject();
            JSONObject structuredQueryObject = new JSONObject();
            JSONObject whereObject = new JSONObject();
            JSONObject selectObject = new JSONObject();
            JSONArray fromArray = new JSONArray();
            JSONObject fieldFilterObject = new JSONObject();
            JSONObject fieldObject = new JSONObject();
            JSONObject valueOject = new JSONObject();
            JSONArray fieldsArray = new JSONArray();
            JSONObject pojedinacniIndexTacnogObject = new JSONObject();
            JSONObject pojedinacniCollectionIdObject = new JSONObject();
            fieldObject.put("fieldPath", "naziv");
            valueOject.put("stringValue", nazivZadanogPitanja);
            fieldFilterObject.put("field", fieldObject);
            fieldFilterObject.put("op", "EQUAL");
            fieldFilterObject.put("value", valueOject);
            whereObject.put("fieldFilter", fieldFilterObject);
            pojedinacniIndexTacnogObject.put("fieldPath", "indexTacnog");
            fieldsArray.put(pojedinacniIndexTacnogObject);
            selectObject.put("fields", fieldsArray);
            pojedinacniCollectionIdObject.put("collectionId", "Pitanja");
            fromArray.put(pojedinacniCollectionIdObject);
            structuredQueryObject.put("where", whereObject);
            structuredQueryObject.put("select", selectObject);
            structuredQueryObject.put("from", fromArray);
            jsonObject.put("structuredQuery", structuredQueryObject);
            String strukturiraniUpit = jsonObject.toString();
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = strukturiraniUpit.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            String rezultujuciJsonString = "";
            try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                rezultujuciJsonString = response.toString();
                rezultujuciJsonString = "{\"documents\": " + rezultujuciJsonString + "}";
                Log.d("ODGOVORPitPoNaz", rezultujuciJsonString);
                JSONObject jsonObjectDobijeni = new JSONObject(rezultujuciJsonString);
                JSONArray dokumentiArray = jsonObjectDobijeni.getJSONArray("documents");
                for (int i = 0; i < dokumentiArray.length(); i++) {
                    JSONObject pojedinacniDokumentObject = dokumentiArray.getJSONObject(i);
                    if (!pojedinacniDokumentObject.has("document")) {
                        resultReceiver.send(PITANJE_NE_POSTOJI, bundle);
                        break;
                    }
                    else {
                        resultReceiver.send(PITANJE_VEC_POSTOJI, bundle);
                        break;
                    }
                }
                Log.d("status", "zavrsilo pitanje po nazivu");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
