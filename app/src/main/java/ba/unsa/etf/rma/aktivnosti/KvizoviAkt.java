package ba.unsa.etf.rma.aktivnosti;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.broadcastreceiveri.InternetBroadcastReceiver;
import ba.unsa.etf.rma.fragmenti.DetailFrag;
import ba.unsa.etf.rma.fragmenti.ListaFrag;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.KategorijeAdapter;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.KvizoviAdapter;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.korisneklase.OsvjezivacPodatakaUBazama;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeKategorijaReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeKvizovaPoKategorijiReceiver;
import ba.unsa.etf.rma.resultreceiveri.UcitavanjeKvizovaReceiver;
import ba.unsa.etf.rma.servisi.UcitavanjeKategorijaService;
import ba.unsa.etf.rma.servisi.UcitavanjeKvizovaPoKategorijiService;
import ba.unsa.etf.rma.servisi.UcitavanjeKvizovaService;
import ba.unsa.etf.rma.sqliteopenhelperi.BazaSQLiteOpenHelper;

public class KvizoviAkt extends AppCompatActivity implements ListaFrag.OnItemClick, DetailFrag.OnItemClick,
        UcitavanjeKvizovaReceiver.Receiver, UcitavanjeKategorijaReceiver.Receiver, UcitavanjeKvizovaPoKategorijiReceiver.Receiver,
        InternetBroadcastReceiver.Aktivnost {

    private boolean ekranSiriOd550dp;
    private String nazivIzabraneKategorije = "";
    private ArrayList<Kategorija> listaPostojecihKategorija = new ArrayList<>();
    private ArrayList<Kviz> listaPostojecihKvizova = new ArrayList<>();
    private ArrayList<Kviz> listaPrikazanihKvizova = new ArrayList<>();
    private Spinner spPostojeceKategorije;
    private ListView lvKvizovi;
    private View footerDodajKviz;
    private KvizoviAdapter kvizoviAdapter;
    private BaseAdapter kategorijeAdapter;
    private FragmentManager fragmentManager;
    private ListaFrag listaFrag;
    private DetailFrag detailFrag;
    private boolean prvoPrikazivanje = true;
    private ProgressDialog progressDialog;
    private boolean dozvoljenoIgranjeKviza = false;
    private long brojMinutaDoSljedecegDogadjaja = 0;
    private InternetBroadcastReceiver internetBroadcastReceiver = new InternetBroadcastReceiver();
    private IntentFilter internetIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    private boolean imaInternetKonekcije;
    private BazaSQLiteOpenHelper bazaSQLiteOpenHelper;
    private boolean trebaOsvjezitiNaPocetku;
    private boolean prvo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prvoPrikazivanje = true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kvizovi_akt);
        progressDialog = new ProgressDialog(KvizoviAkt.this);

        // Na osnovu sirine ekrana se zakljucuje kakav layout prikazati

        utvrdiSirinuEkrana();
        bazaSQLiteOpenHelper = new BazaSQLiteOpenHelper(this, BazaSQLiteOpenHelper.NAZIV_BAZE,
                null, BazaSQLiteOpenHelper.VERZIJA_BAZE);
        postaviEkranNaOsnovuSirine();
    }

    private void utvrdiSirinuEkrana() {
        FrameLayout frameLayoutIzEkrana550dp = (FrameLayout) findViewById(R.id.listPlace);
        if (frameLayoutIzEkrana550dp == null) ekranSiriOd550dp = false;
        else ekranSiriOd550dp = true;
    }

    private void postaviEkranNaOsnovuSirine() {
        if (!ekranSiriOd550dp) inicijalizirajEkran();
        else inicijalizirajEkranW550dp();
    }

    private void inicijalizirajEkran() {
        dobaviWidgete();
        postaviFooterNaListuKvizova();
        inicijalizirajKategorije();
        postaviAdaptere();
        postaviListenerZaSkrolanjeListeKvizova();
        postaviListenerNaKategoriju();
        postaviListenerNaDugiKlikNaKviz();
        postaviListenerNaObicniKlikNaKviz();
        postaviListenerNaFooterDodavanjeKviza();
    }

    private void inicijalizirajEkranW550dp() {
        inicijalizirajKategorije();
        inicijalizirajFragmentManager();
        postaviFragmentLista();
        postaviFragmentDetail();
    }

    private void inicijalizirajFragmentManager() {
        fragmentManager = getSupportFragmentManager();
    }

    private void postaviFragmentLista() {
        FrameLayout listPlace = (FrameLayout) findViewById(R.id.listPlace);
        if (listPlace != null) {
            listaFrag = (ListaFrag) fragmentManager.findFragmentById(R.id.listPlace);
            if (listaFrag == null) {
                listaFrag = new ListaFrag();
                Bundle argumentiFragmentaLista = new Bundle();
                argumentiFragmentaLista.putParcelableArrayList("bundleListaKategorija", listaPostojecihKategorija);
                listaFrag.setArguments(argumentiFragmentaLista);
                fragmentManager.beginTransaction().replace(R.id.listPlace, listaFrag).commit();
            }
        }
    }

    private void postaviFragmentDetail() {
        FrameLayout detailPlace = (FrameLayout) findViewById(R.id.detailPlace);
        if (detailPlace != null) {
            detailFrag = (DetailFrag) fragmentManager.findFragmentById(R.id.detailPlace);
            if (detailFrag == null) {
                detailFrag = new DetailFrag();
                Bundle argumentiFragmentaDetail = new Bundle();
                argumentiFragmentaDetail.putParcelableArrayList("bundleListaPrikazanihKvizova", listaPrikazanihKvizova);
                detailFrag.setArguments(argumentiFragmentaDetail);
                fragmentManager.beginTransaction().replace(R.id.detailPlace, detailFrag).commit();
            }
        }
    }

    private void dobaviWidgete() {
        spPostojeceKategorije = (Spinner) findViewById(R.id.spPostojeceKategorije);
        lvKvizovi = (ListView) findViewById(R.id.lvKvizovi);
        footerDodajKviz = ((LayoutInflater) KvizoviAkt.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.footer_dodaj_kviz, null, false);
    }

    private void postaviFooterNaListuKvizova() {

        // Dodatni element liste kvizova, koji sluzi za pokretanje dodavanja novog kviza

        lvKvizovi.addFooterView(footerDodajKviz);
    }

    private void inicijalizirajKategorije() {
        provjeriInternetKonekciju();
        listaPostojecihKategorija = new ArrayList<>();
        Kategorija kategorijaSvi = new Kategorija();

        // Univerzalna kategorija "Svi" koja sluzi za prikaz svih kvizova, pa i onih koji nemaju kategoriju

        kategorijaSvi.setNaziv(getResources().getString(R.string.specijalna_kategorija_svi));
        kategorijaSvi.setId("976");
        kategorijaSvi.setIdIzFirebasea("ktgrjSvi");
        listaPostojecihKategorija.add(kategorijaSvi);
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage("Ucitavanje...");
            progressDialog.show();
        }
        if (imaInternetKonekcije) ucitajKategorijeIzFireBaseaIOsvjeziEkran();
        else ucitajKategorijeIzSQLiteaIOsvjeziEkran();
    }

    private void ucitajKategorijeIzSQLiteaIOsvjeziEkran() {
        String[] projekcija = new String[] { BazaSQLiteOpenHelper.KATEGORIJA_NAZIV,
        BazaSQLiteOpenHelper.KATEGORIJA_ID_IKONICE };
        SQLiteDatabase db = bazaSQLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(BazaSQLiteOpenHelper.NAZIV_TABELE_KATEGORIJE, projekcija, null,
                null, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                    String primljeniNazivKategorije = cursor.getString
                            (cursor.getColumnIndex(BazaSQLiteOpenHelper.KATEGORIJA_NAZIV));
                    String primljeniIdIkoniceKategorije = cursor.getString
                            (cursor.getColumnIndex(BazaSQLiteOpenHelper.KATEGORIJA_ID_IKONICE));
                    listaPostojecihKategorija.add(new Kategorija(primljeniNazivKategorije, primljeniIdIkoniceKategorije));
                }
            }
        }
        if (cursor != null) cursor.close();
        db.close();
        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (ekranSiriOd550dp) osvjeziFragmentLista();
        else osvjeziPrikazKategorija();
    }

    private void postaviAdaptere() {
        kategorijeAdapter = new KategorijeAdapter(this, listaPostojecihKategorija, getResources());
        spPostojeceKategorije.setAdapter(kategorijeAdapter);
        kvizoviAdapter = new KvizoviAdapter(this, listaPrikazanihKvizova, getResources());
        lvKvizovi.setAdapter(kvizoviAdapter);
    }

    private void postaviListenerZaSkrolanjeListeKvizova() {
        lvKvizovi.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    private void postaviListenerNaKategoriju() {
        spPostojeceKategorije.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("stat", "selected");
                provjeriInternetKonekciju();
                if (imaInternetKonekcije) {
                    if (!prvoPrikazivanje) {
                        if (position == 0) {

                            // Ako je kliknuto na kategoriju "Svi" prikazuju se svi kvizovi
                            listaPostojecihKvizova = new ArrayList<>();
                            listaPrikazanihKvizova = new ArrayList<>();
                            if (!progressDialog.isShowing()) {
                                progressDialog.setMessage("Ucitavanje...");
                                progressDialog.show();
                            }
                            ucitajKvizoveIzFirebaseaIOsvjeziEkran();
                        }
                        else {

                            // Inace, prikazuju se samo kvizovi iz odabrane kategorije
                            listaPostojecihKvizova =  new ArrayList<>();
                            listaPrikazanihKvizova = new ArrayList<>();
                            if (!progressDialog.isShowing()) {
                                progressDialog.setMessage("Ucitavanje...");
                                progressDialog.show();
                            }
                            ucitajKvizoveIzFirebaseaPoKategorijiIOsvjeziEkran(listaPostojecihKategorija.get(position).getIdIzFirebasea());
                        }
                    }
                }
                else {
                    if (position == 0) {

                        // Ako je kliknuto na kategoriju "Svi" prikazuju se svi kvizovi
                        listaPostojecihKvizova = new ArrayList<>();
                        listaPrikazanihKvizova = new ArrayList<>();
                        if (!progressDialog.isShowing()) {
                            progressDialog.setMessage("Ucitavanje...");
                            progressDialog.show();
                        }
                        ucitajKvizoveIzSQLiteaIOsvjeziEkran();
                    }
                    else {

                        // Inace, prikazuju se samo kvizovi iz odabrane kategorije
                        listaPostojecihKvizova =  new ArrayList<>();
                        listaPrikazanihKvizova = new ArrayList<>();
                        if (!progressDialog.isShowing()) {
                            progressDialog.setMessage("Ucitavanje...");
                            progressDialog.show();
                        }
                        ucitajKvizoveIzSQLiteaPoKategorijiIOsvjeziEkran(listaPostojecihKategorija.get(position).getNaziv());
                    }
                }
                if (prvoPrikazivanje) prvoPrikazivanje = false;
                nazivIzabraneKategorije = listaPostojecihKategorija.get(position).getNaziv();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //spPostojeceKategorije.setSelection(0);
                nazivIzabraneKategorije = listaPostojecihKategorija.get(0).getNaziv();
            }
        });
    }

    private void ucitajKvizoveIzSQLiteaIOsvjeziEkran() {
        String[] projekcija = new String[] { BazaSQLiteOpenHelper.KVIZ_NAZIV,
                BazaSQLiteOpenHelper.KVIZ_KATEGORIJA };
        SQLiteDatabase db = bazaSQLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(BazaSQLiteOpenHelper.NAZIV_TABELE_KVIZOVI, projekcija, null,
                null, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                    String primljeniNazivKviza = cursor.getString
                            (cursor.getColumnIndex(BazaSQLiteOpenHelper.KVIZ_NAZIV));
                    String primljeniNazivKategorijeKviza = cursor.getString
                            (cursor.getColumnIndex(BazaSQLiteOpenHelper.KVIZ_KATEGORIJA));
                    Kategorija ucitanaKategorija = nadjiUcitanuKategorijuPoNazivu(primljeniNazivKategorijeKviza);
                    ArrayList<Pitanje> ucitanaPitanja = ucitajPitanjaPoNazivuKvizaIzSQLite(db, primljeniNazivKviza);
                    Kviz noviKviz = new Kviz(primljeniNazivKviza, ucitanaPitanja, ucitanaKategorija);
                    listaPostojecihKvizova.add(noviKviz);
                    listaPrikazanihKvizova.add(noviKviz);
                }
            }
        }
        if (cursor != null) cursor.close();
        db.close();
        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (ekranSiriOd550dp) osvjeziFragmentDetail();
        else osvjeziPrikazKvizova();
    }

    private void ucitajKvizoveIzSQLiteaPoKategorijiIOsvjeziEkran(String nazivKategorije) {
        String[] projekcija = new String[] { BazaSQLiteOpenHelper.KVIZ_NAZIV,
                BazaSQLiteOpenHelper.KVIZ_KATEGORIJA };
        String selekcija = "(" + BazaSQLiteOpenHelper.KVIZ_KATEGORIJA + " = ?)";
        String[] argumentiSelekcije = new String[] { nazivKategorije };
        SQLiteDatabase db = bazaSQLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(BazaSQLiteOpenHelper.NAZIV_TABELE_KVIZOVI, projekcija, selekcija,
                argumentiSelekcije, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                    String primljeniNazivKviza = cursor.getString
                            (cursor.getColumnIndex(BazaSQLiteOpenHelper.KVIZ_NAZIV));
                    String primljeniNazivKategorijeKviza = cursor.getString
                            (cursor.getColumnIndex(BazaSQLiteOpenHelper.KVIZ_KATEGORIJA));
                    Kategorija ucitanaKategorija = nadjiUcitanuKategorijuPoNazivu(primljeniNazivKategorijeKviza);
                    ArrayList<Pitanje> ucitanaPitanja = ucitajPitanjaPoNazivuKvizaIzSQLite(db, primljeniNazivKviza);
                    Kviz noviKviz = new Kviz(primljeniNazivKviza, ucitanaPitanja, ucitanaKategorija);
                    listaPostojecihKvizova.add(noviKviz);
                    listaPrikazanihKvizova.add(noviKviz);
                }
            }
        }
        if (cursor != null) cursor.close();
        db.close();
        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (ekranSiriOd550dp) osvjeziFragmentDetail();
        else osvjeziPrikazKvizova();
    }

    private Kategorija nadjiUcitanuKategorijuPoNazivu(String nazivKategorije) {
        for (Kategorija kategorija : listaPostojecihKategorija) {
            if (kategorija.getNaziv().equals(nazivKategorije)) return kategorija;
        }
        return new Kategorija();
    }

    private ArrayList<Pitanje> ucitajPitanjaPoNazivuKvizaIzSQLite(SQLiteDatabase db, String naziv) {
        ArrayList<String> ucitaniNaziviPitanja = ucitajNazivePitanjaPoNazivuKvizaIzSQLite(db, naziv);
        return ucitajPitanjaPoNazivimaIzSQLite(db, ucitaniNaziviPitanja);
    }

    private ArrayList<String> ucitajNazivePitanjaPoNazivuKvizaIzSQLite(SQLiteDatabase db, String nazivKviza) {
        ArrayList<String> povratna = new ArrayList<>();
        String[] projekcijaPitanjeUKvizu = new String[] { BazaSQLiteOpenHelper.PITANJE_U_KVIZU_PITANJE };
        String selekcijaPitanjeUKvizu = "(" + BazaSQLiteOpenHelper.PITANJE_U_KVIZU_KVIZ + " = ?)";
        String[] argumentiSelekcijePitanjeUKvizu = new String[] { nazivKviza };
        Cursor cursor = db.query(BazaSQLiteOpenHelper.NAZIV_TABELE_PITANJE_U_KVIZU, projekcijaPitanjeUKvizu, selekcijaPitanjeUKvizu,
                argumentiSelekcijePitanjeUKvizu, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                    String primljeniNazivPitanja = cursor.getString
                            (cursor.getColumnIndex(BazaSQLiteOpenHelper.PITANJE_U_KVIZU_PITANJE));
                    povratna.add(primljeniNazivPitanja);
                }
            }
        }
        if (cursor != null) cursor.close();
        return povratna;
    }

    private ArrayList<Pitanje> ucitajPitanjaPoNazivimaIzSQLite(SQLiteDatabase db, ArrayList<String> nazivi) {
        ArrayList<Pitanje> povratna = new ArrayList<>();
        for (String naziv : nazivi) {
            Pitanje nadjenoPitanjePoNazivu = ucitajPitanjePoNazivuIzSQLite(db, naziv);
            povratna.add(nadjenoPitanjePoNazivu);
        }
        return povratna;
    }

    private Pitanje ucitajPitanjePoNazivuIzSQLite(SQLiteDatabase db, String nazivPitanja) {
        Pitanje povratno = new Pitanje();
        String[] projekcijaPitanje = new String[] { BazaSQLiteOpenHelper.PITANJE_TACAN };
        String selekcijaPitanje = "(" + BazaSQLiteOpenHelper.PITANJE_NAZIV + " = ?)";
        String[] argumentiSelekcijePitanje = new String[] { nazivPitanja };
        Cursor cursorPitanje = db.query(BazaSQLiteOpenHelper.NAZIV_TABELE_PITANJA, projekcijaPitanje,
                selekcijaPitanje, argumentiSelekcijePitanje, null, null, null);
        if (cursorPitanje != null) {
            if (cursorPitanje.getCount() != 0) {
                cursorPitanje.moveToFirst();
                String primljeniTacan = cursorPitanje.getString
                        (cursorPitanje.getColumnIndex(BazaSQLiteOpenHelper.PITANJE_TACAN));
                ArrayList<String> primljeniOdgovori = ucitajOdgovorePoNazivuPitanjaIzSQLite(db, nazivPitanja);
                povratno.setNaziv(nazivPitanja);
                povratno.setTekstPitanja(nazivPitanja);
                povratno.setTacan(primljeniTacan);
                povratno.setOdgovori(primljeniOdgovori);
            }
        }
        if (cursorPitanje != null) cursorPitanje.close();
        return povratno;
    }

    private ArrayList<String> ucitajOdgovorePoNazivuPitanjaIzSQLite(SQLiteDatabase db, String nazivPitanja) {
        ArrayList<String> povratna = new ArrayList<>();
        String[] projekcijaOdgovori = new String[] { BazaSQLiteOpenHelper.ODGOVOR_TEKST};
        String selekcijaOdgovori = "(" + BazaSQLiteOpenHelper.ODGOVOR_PITANJE + " = ?)";
        String[] argumentiSelekcijeOdgovori = new String[] { nazivPitanja };
        Cursor cursor = db.query(BazaSQLiteOpenHelper.NAZIV_TABELE_ODGOVORI, projekcijaOdgovori, selekcijaOdgovori,
                argumentiSelekcijeOdgovori, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                    String primljeniTekstOdgovora = cursor.getString
                            (cursor.getColumnIndex(BazaSQLiteOpenHelper.ODGOVOR_TEKST));
                    povratna.add(primljeniTekstOdgovora);
                }
            }
        }
        if (cursor != null) cursor.close();
        return povratna;
    }

    private void provjeriInternetKonekciju() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            imaInternetKonekcije = false;
            return;
        }
        if (networkInfo.isConnected()) {
            imaInternetKonekcije = true;
        }
        else {
            imaInternetKonekcije = false;
        }
    }

    private void osvjeziPrikazKvizova() {
        BaseAdapter noviAdapter = new KvizoviAdapter(this, listaPrikazanihKvizova, getResources());
        lvKvizovi.setAdapter(noviAdapter);
    }

    private void ucitajKvizoveIzFirebaseaPoKategorijiIOsvjeziEkran(String idDokumentaKategorije) {
        UcitavanjeKvizovaPoKategorijiReceiver ucitavanjeKvizovaPoKategorijiReceiver = new UcitavanjeKvizovaPoKategorijiReceiver(new Handler());
        ucitavanjeKvizovaPoKategorijiReceiver.setReceiver(this);
        Intent intent = new Intent(this, UcitavanjeKvizovaPoKategorijiService.class);
        intent.putExtra("extraUcitavanjeKvizovaPoKategorijiReceiver", ucitavanjeKvizovaPoKategorijiReceiver);
        intent.putExtra("extraIdZadaneKategorijeIzFirebasea", idDokumentaKategorije);
        startService(intent);
    }

    private void postaviListenerNaDugiKlikNaKviz() {
        lvKvizovi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (imaInternetKonekcije) {
                    Intent intentUredjivanjeKviza = new Intent(KvizoviAkt.this, DodajKvizAkt.class);

                    /* Preko booleana "extraDodavanjeNovogKviza" salje se informacija aktivnosti
                    DodajKvizAkt da li se treba uredjivati postojeci kviz ili dodavati novi, a u ovom
                    slucaju kliknuto je na postojeci kviz, tako da se ne dodaje novi nego uredjuje postojeci */

                    intentUredjivanjeKviza.putExtra("extraDodavanjeNovogKviza", false);
                    Kviz kvizZaUredjivanje = listaPrikazanihKvizova.get(position);

                    // Aktivnosti DodajKvizAkt salju se postojeci podaci o kvizu za uredjivanje

                    intentUredjivanjeKviza.putExtra("extraKvizZaUredjivanje", (Serializable) kvizZaUredjivanje);
                    intentUredjivanjeKviza.putExtra("extraPozicijaKvizaUListi", position);
                    intentUredjivanjeKviza.putParcelableArrayListExtra("extraListaKategorija",
                            listaPostojecihKategorija);

                    /* Aktivnosti DodajKvizAkt salje se i lista svih ostalih kvizova da bi se moglo zabraniti
                    ime kviza koje vec postoji, jer je ime kviza jedinstveno */

                    ArrayList<Kviz> listaOstalihKvizova = new ArrayList<>();
                    for (Kviz kviz : listaPostojecihKvizova) {
                        if (!kviz.getNaziv().equals(kvizZaUredjivanje.getNaziv())) {
                            listaOstalihKvizova.add(kviz);
                        }
                    }
                    intentUredjivanjeKviza.putParcelableArrayListExtra("extraListaDosadasnjihKvizova", listaOstalihKvizova);
                    startActivityForResult(intentUredjivanjeKviza, 2);
                }
                return true;
            }
        });
    }

    private void postaviListenerNaObicniKlikNaKviz() {
        lvKvizovi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Kviz kvizZaIgranje = listaPrikazanihKvizova.get(position);
                int brojPitanjaKvizaZaIgranje = kvizZaIgranje.getPitanja().size();
                dozvoljenoIgranjeKviza = true;
                provjeriPostojanjeDogadjajaUKalendaru(brojPitanjaKvizaZaIgranje);
                if (!dozvoljenoIgranjeKviza) {
                    prikaziAlertDogadjajUKalendaru(brojMinutaDoSljedecegDogadjaja);
                    return;
                }
                Intent intentIgranjeKviza = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);

                // Aktivnosti IgrajKvizAkt salju se postojeci podaci o kvizu za igranje

                intentIgranjeKviza.putExtra("extraKvizZaIgranje", (Serializable) kvizZaIgranje);
                startActivityForResult(intentIgranjeKviza, 6);
            }
        });
    }

    private void provjeriPostojanjeDogadjajaUKalendaru(int brojPitanjaKviza) {
        if (brojPitanjaKviza == 0) return;
        int granicnoVrijemeUMinutama = brojPitanjaKviza / 2;
        if (jeLiNeparanBroj(brojPitanjaKviza)) granicnoVrijemeUMinutama += 1;
        Calendar kalendar = null;
        kalendar = new GregorianCalendar();
        long trenutnoVrijemeMillis = System.currentTimeMillis();
        kalendar.setTimeInMillis(trenutnoVrijemeMillis);
        kalendar.add(Calendar.MINUTE, granicnoVrijemeUMinutama);
        long granicniTrenutakMillis = kalendar.getTimeInMillis();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALENDAR}, 1);
        }
        Uri uri = CalendarContract.Events.CONTENT_URI;
        String[] projekcija = new String[] {
                CalendarContract.Events._ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DTSTART
        };
        String selekcija = "((" + CalendarContract.Events.DTSTART + " < ?) AND (" +
                CalendarContract.Events.DTSTART + " > ?))";
        String[] argumentiSelekcije = new String[] {
                String.valueOf(granicniTrenutakMillis),
                String.valueOf(trenutnoVrijemeMillis)
        };
        Cursor kursor = getContentResolver().query(uri, projekcija, selekcija, argumentiSelekcije,
                CalendarContract.Events.DTSTART);
        if (kursor != null) {
            if (kursor.getCount() != 0) {
                dozvoljenoIgranjeKviza = false;
                kursor.moveToFirst();
                long ucitanoVrijemeEventaMillis = kursor.getLong(kursor.getColumnIndex(CalendarContract.Events.DTSTART));
                long vrijemeOdSadDoEventaMillis = ucitanoVrijemeEventaMillis - trenutnoVrijemeMillis;
                brojMinutaDoSljedecegDogadjaja = TimeUnit.MILLISECONDS.toMinutes(vrijemeOdSadDoEventaMillis);
            }
        }
        if (kursor != null) kursor.close();
    }

    private boolean jeLiNeparanBroj(int brojZaTestiranje) {
        return brojZaTestiranje % 2 != 0;
    }

    private void prikaziAlertDogadjajUKalendaru(long brojMinutaDoDogadjaja) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Imate događaj u kalendaru za ");
        stringBuilder.append(brojMinutaDoDogadjaja);
        stringBuilder.append(" minuta!");
        String porukaZaAlertDialog = stringBuilder.toString();
        alertDialogBuilder.setMessage(porukaZaAlertDialog);
        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dijalogZaGresku = alertDialogBuilder.create();
        dijalogZaGresku.show();
    }

    private void postaviListenerNaFooterDodavanjeKviza() {
        footerDodajKviz.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (imaInternetKonekcije) {
                    Intent intentDodavanjeKviza = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                    intentDodavanjeKviza.putExtra("extraDodavanjeNovogKviza",
                            true);
                    intentDodavanjeKviza.putParcelableArrayListExtra("extraListaKategorija",
                            listaPostojecihKategorija);
                    intentDodavanjeKviza.putParcelableArrayListExtra("extraListaDosadasnjihKvizova", listaPostojecihKvizova);
                    startActivityForResult(intentDodavanjeKviza, 1);
                }
                return true;
            }
        });
    }

    private void ucitajKvizoveIzFirebaseaIOsvjeziEkran() {
        UcitavanjeKvizovaReceiver ucitavanjeKvizovaReceiver = new UcitavanjeKvizovaReceiver(new Handler());
        ucitavanjeKvizovaReceiver.setReceiver(this);
        Intent intent = new Intent(this, UcitavanjeKvizovaService.class);
        intent.putExtra("extraUcitavanjeKvizovaReceiver", ucitavanjeKvizovaReceiver);
        startService(intent);
    }

    private void ucitajKategorijeIzFireBaseaIOsvjeziEkran() {
        UcitavanjeKategorijaReceiver ucitavanjeKategorijaReceiver = new UcitavanjeKategorijaReceiver(new Handler());
        ucitavanjeKategorijaReceiver.setReceiver(this);
        Intent intent = new Intent(this, UcitavanjeKategorijaService.class);
        intent.putExtra("extraUcitavanjeKategorijaReceiver", ucitavanjeKategorijaReceiver);
        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {

            /* Slucaj kada je aktivnost DodajKvizAkt pozvana i vracena nazad na ovu s ciljem dodavanja
            novog kviza */

            if (resultCode == RESULT_OK) {
                inicijalizirajKategorije();
            }
            else if (resultCode == RESULT_CANCELED) {

                /* Ako se korisnik vratio pritiskom na Back, potrebno je eventualno azurirati listu kategorija
                ako su dodane neke nove */

                inicijalizirajKategorije();
            }
        }
        else if (requestCode == 2) {

            /* Slucaj kada je aktivnost DodajKvizAkt pozvana i vracena nazad na ovu s ciljem izmjene
            postojeceg kviza */

            if (resultCode == RESULT_OK) {
                inicijalizirajKategorije();
            }
            else if (resultCode == RESULT_CANCELED) {

                /* Ako se korisnik vratio pritiskom na Back, potrebno je eventualno azurirati listu kategorija
                ako su dodane neke nove */

                inicijalizirajKategorije();
            }
        }
        else if (requestCode == 6) {

            /* Slucaj kada je aktivnost IgrajKvizAkt pozvana */

            if (resultCode == RESULT_OK) {
                inicijalizirajKategorije();
            }
            else if (resultCode == RESULT_CANCELED) {
                inicijalizirajKategorije();
            }
        }
    }

    private void osvjeziPodatkeNaEkranu() {
        izbrisiFiktivnuKategoriju();
        if (ekranSiriOd550dp) {
            osvjeziFragmentLista();
            osvjeziFragmentDetail();
        }
        else {
            osvjeziPrikazKategorija();
            osvjeziPrikazKvizova();
        }
    }

    private void osvjeziFragmentLista() {
        listaFrag = new ListaFrag();
        Bundle argumentiFragmentaLista = new Bundle();
        argumentiFragmentaLista.putParcelableArrayList("bundleListaKategorija", listaPostojecihKategorija);
        listaFrag.setArguments(argumentiFragmentaLista);
        fragmentManager.beginTransaction().replace(R.id.listPlace, listaFrag).commitAllowingStateLoss();
    }

    private void osvjeziFragmentDetail() {
        detailFrag = new DetailFrag();
        Bundle argumentiFragmentaDetail = new Bundle();
        argumentiFragmentaDetail.putParcelableArrayList("bundleListaPrikazanihKvizova", listaPrikazanihKvizova);
        detailFrag.setArguments(argumentiFragmentaDetail);
        fragmentManager.beginTransaction().replace(R.id.detailPlace, detailFrag).commitAllowingStateLoss();
    }

    private void izbrisiFiktivnuKategoriju() {
        int pozicijaZaBrisanje = listaPostojecihKategorija.size() - 1;
        if (listaPostojecihKategorija.get(pozicijaZaBrisanje).getNaziv().equals("Dodaj kategoriju"))
            listaPostojecihKategorija.remove(pozicijaZaBrisanje);
    }

    private void osvjeziPrikazKategorija() {
        kategorijeAdapter = new KategorijeAdapter(this, listaPostojecihKategorija, getResources());
        spPostojeceKategorije.setAdapter(kategorijeAdapter);
    }

    @Override
    public void onKategorijaClicked(int position) {
        if (position >= listaPostojecihKategorija.size()) return;
        if (position == 0) {

            // Ako je kliknuto na kategoriju "Svi" prikazuju se svi kvizovi

            listaPostojecihKvizova = new ArrayList<>();
            listaPrikazanihKvizova = new ArrayList<>();
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Ucitavanje...");
                progressDialog.show();
            }
            ucitajKvizoveIzFirebaseaIOsvjeziEkran();
        }
        else {

            // Inace, prikazuju se samo kvizovi iz odabrane kategorije

            listaPostojecihKvizova =  new ArrayList<>();
            listaPrikazanihKvizova = new ArrayList<>();
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Ucitavanje...");
                progressDialog.show();
            }
            ucitajKvizoveIzFirebaseaPoKategorijiIOsvjeziEkran(listaPostojecihKategorija.get(position).getIdIzFirebasea());
        }
        osvjeziPodatkeNaEkranu();
        nazivIzabraneKategorije = listaPostojecihKategorija.get(position).getNaziv();
    }

    @Override
    public void onKvizLongClicked(int position) {
        Intent intentUredjivanjeKviza = new Intent(KvizoviAkt.this, DodajKvizAkt.class);

                /* Preko booleana "extraDodavanjeNovogKviza" salje se informacija aktivnosti
                DodajKvizAkt da li se treba uredjivati postojeci kviz ili dodavati novi, a u ovom
                slucaju kliknuto je na postojeci kviz, tako da se ne dodaje novi nego uredjuje postojeci */

        intentUredjivanjeKviza.putExtra("extraDodavanjeNovogKviza", false);
        if (position >= listaPrikazanihKvizova.size()) return;
        Kviz kvizZaUredjivanje = listaPrikazanihKvizova.get(position);

        // Aktivnosti DodajKvizAkt salju se postojeci podaci o kvizu za uredjivanje

        intentUredjivanjeKviza.putExtra("extraKvizZaUredjivanje", (Serializable) kvizZaUredjivanje);
        intentUredjivanjeKviza.putExtra("extraPozicijaKvizaUListi", position);
        intentUredjivanjeKviza.putParcelableArrayListExtra("extraListaKategorija",
                listaPostojecihKategorija);

                /* Aktivnosti DodajKvizAkt salje se i lista svih ostalih kvizova da bi se moglo zabraniti
                ime kviza koje vec postoji, jer je ime kviza jedinstveno */

        ArrayList<Kviz> listaOstalihKvizova = new ArrayList<>();
        for (Kviz kviz : listaPostojecihKvizova) {
            if (!kviz.getNaziv().equals(kvizZaUredjivanje.getNaziv())) {
                listaOstalihKvizova.add(kviz);
            }
        }
        intentUredjivanjeKviza.putParcelableArrayListExtra("extraListaDosadasnjihKvizova", listaOstalihKvizova);
        startActivityForResult(intentUredjivanjeKviza, 2);
    }

    @Override
    public void onKvizClickedIgrajKviz(int position) {
        Intent intentIgranjeKviza = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
        if (position >= listaPrikazanihKvizova.size()) return;
        Kviz kvizZaIgranje = listaPrikazanihKvizova.get(position);

        // Aktivnosti IgrajKvizAkt salju se postojeci podaci o kvizu za igranje

        intentIgranjeKviza.putExtra("extraKvizZaIgranje", (Serializable) kvizZaIgranje);
        startActivityForResult(intentIgranjeKviza, 6);
    }

    @Override
    public void onKvizClickedDodajKviz() {
        Intent intentDodavanjeKviza = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
        intentDodavanjeKviza.putExtra("extraDodavanjeNovogKviza",
                true);
        intentDodavanjeKviza.putParcelableArrayListExtra("extraListaKategorija",
                listaPostojecihKategorija);
        intentDodavanjeKviza.putParcelableArrayListExtra("extraListaDosadasnjihKvizova", listaPostojecihKvizova);
        startActivityForResult(intentDodavanjeKviza, 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prvo = true;
        trebaOsvjezitiNaPocetku = true;
        internetBroadcastReceiver.setAktivnost(this);
        registerReceiver(internetBroadcastReceiver, internetIntentFilter);
        if (ekranSiriOd550dp) onKategorijaClicked(0);
    }

    @Override
    public void naPrimanjeUcitanihKvizova(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeKvizovaService.KVIZOVI_UCITANI) {
            if (resultData.containsKey("extraUcitaniKvizovi")) {
                ArrayList<Kviz> primljeniKvizovi = resultData.getParcelableArrayList("extraUcitaniKvizovi");
                if (primljeniKvizovi != null) {
                    listaPostojecihKvizova = new ArrayList<>();
                    listaPrikazanihKvizova = new ArrayList<>();
                    listaPostojecihKvizova.addAll(primljeniKvizovi);
                    listaPrikazanihKvizova.addAll(listaPostojecihKvizova);
                    if (ekranSiriOd550dp) osvjeziFragmentDetail();
                    else osvjeziPrikazKvizova();
                }
            }
        }
        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (trebaOsvjezitiNaPocetku) {
            trebaOsvjezitiNaPocetku = false;
            if (imaInternetKonekcije) {
                new OsvjezivacPodatakaUBazama(this).azurirajStanjeUSQLiteuIFirebaseu();
            }
        }
    }

    @Override
    public void naPrimanjeUcitanihKategorija(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeKategorijaService.KATEGORIJE_UCITANE) {
            if (resultData.containsKey("extraUcitaneKategorije")) {
                Log.d("status", "ima kljuc ucitane kat");
                ArrayList<Kategorija> primljeneKategorije = resultData.getParcelableArrayList("extraUcitaneKategorije");
                if (primljeneKategorije != null) {
                    listaPostojecihKategorija.addAll(primljeneKategorije);
                    if (ekranSiriOd550dp) osvjeziFragmentLista();
                    else osvjeziPrikazKategorija();
                }
            }
        }
    }

    @Override
    public void naPrimanjeUcitanihKvizovaPoKategoriji(int resultCode, Bundle resultData) {
        if (resultCode == UcitavanjeKvizovaPoKategorijiService.KVIZOVI_UCITANI_PO_KATEGORIJI) {
            if (resultData.containsKey("extraUcitaniKvizoviPoKategoriji")) {
                ArrayList<Kviz> primljeniKvizovi = resultData.getParcelableArrayList("extraUcitaniKvizoviPoKategoriji");
                if (primljeniKvizovi != null) {
                    listaPrikazanihKvizova = new ArrayList<>();
                    listaPostojecihKvizova = new ArrayList<>();
                    listaPostojecihKvizova.addAll(primljeniKvizovi);
                    listaPrikazanihKvizova.addAll(listaPostojecihKvizova);
                    if (ekranSiriOd550dp) osvjeziFragmentDetail();
                    else osvjeziPrikazKvizova();
                }
            }
        }
        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (trebaOsvjezitiNaPocetku) {
            trebaOsvjezitiNaPocetku = false;
            if (imaInternetKonekcije) {
                new OsvjezivacPodatakaUBazama(this).azurirajStanjeUSQLiteuIFirebaseu();
            }
        }
    }

    @Override
    protected void onPause() {
        unregisterReceiver(internetBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void izmijeniStatusKonekcije(boolean imaKonekcije) {
        imaInternetKonekcije = imaKonekcije;
        if (prvo) {
            prvo = false;
            return;
        }
        if (imaKonekcije) new OsvjezivacPodatakaUBazama(this).azurirajStanjeUSQLiteuIFirebaseu();
    }
}