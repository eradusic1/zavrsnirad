package ba.unsa.etf.rma;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ListView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static ba.unsa.etf.rma.ActivityGetter.getCurrentActivity;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class DodajKvizTest {
    @Rule
    public ActivityTestRule<KvizoviAkt> activityRule
            = new ActivityTestRule<>(KvizoviAkt.class);

    @Test
    public void dodajKvizEmpty() {
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int numItems1 = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(numItems1 - 1).perform(longClick());

        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        Espresso.closeSoftKeyboard();
        int numItems = ((ListView) getCurrentActivity().findViewById(R.id.lvDodanaPitanja)).getAdapter().getCount();
        assertTrue(numItems == 1);

        onView(withId(R.id.btnDodajKviz)).perform(click());
        onView(withId(R.id.btnDodajKviz)).check(matches(isDisplayed()));
    }

    @Test
    public void dodajKvizNoTitle() {
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int numItems1 = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(numItems1 - 1).perform(longClick());

        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        Espresso.closeSoftKeyboard();
        onData(anything()).inAdapterView(withId(R.id.lvMogucaPitanja)).atPosition(0).perform(click());
        onData(anything()).inAdapterView(withId(R.id.lvMogucaPitanja)).atPosition(0).perform(click());

        int numItems = ((ListView) getCurrentActivity().findViewById(R.id.lvDodanaPitanja)).getAdapter().getCount();
        assertTrue(numItems == 3);

        onView(withId(R.id.btnDodajKviz)).perform(click());
        onView(withId(R.id.btnDodajKviz)).check(matches(isDisplayed()));
    }

    @Test
    public void removeQuestions() {
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int numItems1 = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(numItems1 - 1).perform(longClick());

        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        Espresso.closeSoftKeyboard();
        onData(anything()).inAdapterView(withId(R.id.lvMogucaPitanja)).atPosition(0).perform(click());
        onData(anything()).inAdapterView(withId(R.id.lvMogucaPitanja)).atPosition(0).perform(click());
        onData(anything()).inAdapterView(withId(R.id.lvMogucaPitanja)).atPosition(0).perform(click());

        int numItems = ((ListView) getCurrentActivity().findViewById(R.id.lvDodanaPitanja)).getAdapter().getCount();
        assertTrue(numItems == 4);
        onData(anything()).inAdapterView(withId(R.id.lvDodanaPitanja)).atPosition(0).perform(click());
        onData(anything()).inAdapterView(withId(R.id.lvDodanaPitanja)).atPosition(0).perform(click());
        numItems = ((ListView) getCurrentActivity().findViewById(R.id.lvDodanaPitanja)).getAdapter().getCount();
        assertTrue(numItems == 2);

        onView(withId(R.id.btnDodajKviz)).perform(click());
        onView(withId(R.id.btnDodajKviz)).check(matches(isDisplayed()));
    }

    @Test
    public void dodajKvizExisting() {
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int numItems1 = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(numItems1 - 1).perform(longClick());

        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        onView(withId(R.id.etNaziv)).perform(typeText("kviz1"));
        Espresso.closeSoftKeyboard();
        onData(anything()).inAdapterView(withId(R.id.lvMogucaPitanja)).atPosition(0).perform(click());
        onData(anything()).inAdapterView(withId(R.id.lvMogucaPitanja)).atPosition(0).perform(click());

        int numItems = ((ListView) getCurrentActivity().findViewById(R.id.lvDodanaPitanja)).getAdapter().getCount();
        assertTrue(numItems == 3);

        onView(withId(R.id.btnDodajKviz)).perform(click());
        onView(withText("Uneseni kviz već postoji!"))
                .inRoot(withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }

  /*  @Test
    public void dodajKviz() {
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int numItems1 = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(numItems1 - 1).perform(longClick());

        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        onView(withId(R.id.etNaziv)).perform(typeText("kvizTestni"));
        Espresso.closeSoftKeyboard();
        onData(anything()).inAdapterView(withId(R.id.lvMogucaPitanja)).atPosition(0).perform(click());

        int numItems = ((ListView) getCurrentActivity().findViewById(R.id.lvDodanaPitanja)).getAdapter().getCount();
        assertTrue(numItems == 2);

        onView(withId(R.id.btnDodajKviz)).perform(click());
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.lvKvizovi)).check(matches(isDisplayed()));
        int numItems2 = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();
        assertTrue(numItems2 > numItems1);

    }
*/
}
