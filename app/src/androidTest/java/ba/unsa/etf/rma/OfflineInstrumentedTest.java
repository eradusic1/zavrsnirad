package ba.unsa.etf.rma;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ListView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

import static org.hamcrest.CoreMatchers.anything;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class OfflineInstrumentedTest {

    @Rule
    public ActivityTestRule<KvizoviAkt> activityRule
            = new ActivityTestRule<>(KvizoviAkt.class);
 /*   @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("ba.unsa.etf.rma", appContext.getPackageName());
    }
    */
    @Test
    public void disableEditQuizWifiOff() {
        //try long click on list element
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(0).perform(longClick());

        //check if list is still visible
        onView(withId(R.id.lvKvizovi)).check(matches(isDisplayed()));
    }
    @Test
    public void disableDodajKviz(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int numItems = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(numItems-1).perform(longClick());
        onView(withId(R.id.lvKvizovi)).check(matches(isDisplayed()));
    }
}
