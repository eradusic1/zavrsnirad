package ba.unsa.etf.rma;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.PositionAssertions.isCompletelyAbove;
import static android.support.test.espresso.assertion.PositionAssertions.isCompletelyBelow;
import static android.support.test.espresso.assertion.PositionAssertions.isLeftOf;
import static android.support.test.espresso.assertion.PositionAssertions.isRightOf;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.anything;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LandscapeInstrumentedTest {
    @Rule
    public ActivityTestRule<KvizoviAkt> activityRule
            = new ActivityTestRule<>(KvizoviAkt.class);

    @Test
    public void kvizoviAktLayoutLandscape() {
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.listaKategorija)).check(isLeftOf(withId(R.id.gridKvizovi)));
        onView(withId(R.id.gridKvizovi)).check(isRightOf(withId(R.id.listaKategorija)));

    }

    @Test
    public void igrajKvizAktLayoutLandscape() {
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onData(anything()).inAdapterView(withId(R.id.gridKvizovi)).atPosition(1).perform(click());
        onView(withId(R.id.pitanjePlace)).check(isCompletelyBelow(withId(R.id.informacijePlace)));
        onView(withId(R.id.informacijePlace)).check(isCompletelyAbove(withId(R.id.pitanjePlace)));
    }


}
