package ba.unsa.etf.rma;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;
import android.widget.ListView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.core.AllOf.allOf;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class OnlineInstrumentedTest {
    @Rule
    public ActivityTestRule<KvizoviAkt> activityRule
            = new ActivityTestRule<>(KvizoviAkt.class);

    @Test
    public void enableEditQuizWifiOn() {
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //try long click on list element
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(0).perform(longClick());

        //check if list is still visible
        onView(withId(R.id.etNaziv)).check(matches(isDisplayed()));
    }

    @Test
    public void filterTests(){
       try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.spPostojeceKategorije)).perform(click());
        onView(allOf(withId(R.id.tvNazivKategorije), withText("probnaKategorija"))).perform(click());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int numItems = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();

        assertTrue("Two elements in listview", numItems==2);
    }
    @Test
    public void disablePlayingDodajKviz(){
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int numItems = ((ListView) activityRule.getActivity().findViewById(R.id.lvKvizovi)).getAdapter().getCount();
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(numItems-1).perform(click());

        onView(withId(R.id.lvKvizovi)).check(matches(isDisplayed()));

        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(numItems-1).perform(longClick());

        onView(withId(R.id.btnDodajKviz)).check(matches(isDisplayed()));
    }


}

