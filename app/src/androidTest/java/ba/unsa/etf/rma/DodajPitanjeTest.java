package ba.unsa.etf.rma;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ListView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ba.unsa.etf.rma.aktivnosti.DodajPitanjeAkt;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class DodajPitanjeTest {
    @Rule
    public ActivityTestRule<DodajPitanjeAkt> activityRule
            = new ActivityTestRule<>(DodajPitanjeAkt.class);

    @Test
    public void dodajPitanjeEmpty(){
        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        onView(withId(R.id.etOdgovor)).check(matches(withText("")));
        onView(withId(R.id.btnDodajPitanje)).perform(click());
        onView(withId(R.id.etOdgovor)).check(matches(isDisplayed()));
    }

    @Test
    public void dodajPitanjeNoCorrectAnswer(){
        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        onView(withId(R.id.etOdgovor)).check(matches(withText("")));

        onView(withId(R.id.etNaziv)).perform(typeText("Test Naziv"));
        onView(withId(R.id.etOdgovor)).perform(typeText("Test Odg"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.btnDodajOdgovor)).perform(click());

        onView(withId(R.id.btnDodajPitanje)).perform(click());
        onView(withId(R.id.etOdgovor)).check(matches(isDisplayed()));
    }

    @Test
    public void dodajPitanjeNoTitle(){
        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        onView(withId(R.id.etOdgovor)).check(matches(withText("")));

        onView(withId(R.id.etOdgovor)).perform(typeText("Test Odg"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajOdgovor)).perform(click());
        onView(withId(R.id.etOdgovor)).perform(typeText("Tacni"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajTacan)).perform(click());

        onView(withId(R.id.btnDodajPitanje)).perform(click());
        onView(withId(R.id.etOdgovor)).check(matches(isDisplayed()));
    }

    @Test
    public void dodajPitanjeDeleteAnswerFromList(){
        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        onView(withId(R.id.etOdgovor)).check(matches(withText("")));

        onView(withId(R.id.etNaziv)).perform(typeText("Test Naziv"));
        onView(withId(R.id.etOdgovor)).perform(typeText("Test Odg1"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajOdgovor)).perform(click());
        onView(withId(R.id.etOdgovor)).perform(typeText("Test Odg2"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajOdgovor)).perform(click());
        onView(withId(R.id.etOdgovor)).perform(typeText("Test Odg3"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajOdgovor)).perform(click());

        int numItems = ((ListView) activityRule.getActivity().findViewById(R.id.lvOdgovori)).getAdapter().getCount();
        assertTrue(numItems==3);

        onData(anything()).inAdapterView(withId(R.id.lvOdgovori)).atPosition(numItems-1).perform(click());
        onData(anything()).inAdapterView(withId(R.id.lvOdgovori)).atPosition(0).perform(click());

        numItems = ((ListView) activityRule.getActivity().findViewById(R.id.lvOdgovori)).getAdapter().getCount();
        assertTrue(numItems==1);
    }

    @Test
    public void dodajPitanjeDeleteCorrectAnswerFromList(){
        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        onView(withId(R.id.etOdgovor)).check(matches(withText("")));

        onView(withId(R.id.etNaziv)).perform(typeText("Test Naziv"));
        onView(withId(R.id.etOdgovor)).perform(typeText("Tacni Odg"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajTacan)).perform(click());
        onView(withId(R.id.etOdgovor)).perform(typeText("Test Odg2"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajOdgovor)).perform(click());
        onView(withId(R.id.etOdgovor)).perform(typeText("Test Odg3"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajOdgovor)).perform(click());

        int numItems = ((ListView) activityRule.getActivity().findViewById(R.id.lvOdgovori)).getAdapter().getCount();
        assertTrue(numItems==3);

        onData(anything()).inAdapterView(withId(R.id.lvOdgovori)).atPosition(0).perform(click());

        numItems = ((ListView) activityRule.getActivity().findViewById(R.id.lvOdgovori)).getAdapter().getCount();
        assertTrue(numItems==2);

        onView(withId(R.id.btnDodajPitanje)).perform(click());
        onView(withId(R.id.etOdgovor)).check(matches(isDisplayed()));
    }

    @Test
    public void dodajPitanjeExistingTitle(){
        onView(withId(R.id.etNaziv)).check(matches(withText("")));
        onView(withId(R.id.etOdgovor)).check(matches(withText("")));

        onView(withId(R.id.etNaziv)).perform(typeText("pitanje1"));
        onView(withId(R.id.etOdgovor)).perform(typeText("Tacni Odg"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajTacan)).perform(click());
        onView(withId(R.id.etOdgovor)).perform(typeText("Test Odg2"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.btnDodajOdgovor)).perform(click());
        onView(withId(R.id.btnDodajPitanje)).perform(click());

        onView(withText("Uneseno pitanje već postoji!"))
                .inRoot(withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }
}
