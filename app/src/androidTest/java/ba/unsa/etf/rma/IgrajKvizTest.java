package ba.unsa.etf.rma;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.PositionAssertions.isCompletelyAbove;
import static android.support.test.espresso.assertion.PositionAssertions.isCompletelyBelow;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static ba.unsa.etf.rma.ActivityGetter.getCurrentActivity;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class IgrajKvizTest {
    @Rule
    public ActivityTestRule<KvizoviAkt> activityRule
            = new ActivityTestRule<>(KvizoviAkt.class);

    /*@Test
    public void igrajKvizCheckAlarm() {
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Toast someToast = Toast.makeText(getCurrentActivity(), "Alarm set 2 minutes from now.", Toast.LENGTH_LONG);
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(4).perform(click());
        //onView(withText("Alarm set")).inRoot(isPlatformPopup()).check(matches(isDisplayed()));
        assertTrue(someToast.getView().isShown());
    }*/

    @Test
    public void igrajKvizFragmentPosition() {
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(4).perform(click());
        onView(withId(R.id.pitanjePlace)).check(isCompletelyBelow(withId(R.id.informacijePlace)));
        onView(withId(R.id.informacijePlace)).check(isCompletelyAbove(withId(R.id.pitanjePlace)));
    }

    @Test
    public void igrajKvizCheckPreostala() {
        try {
            Thread.sleep(50000);

            onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(4).perform(click());
            String num1 = ((TextView) getCurrentActivity().findViewById(R.id.infBrojPreostalihPitanja)).getText().toString();
            int br1 = Integer.parseInt(num1);
            onData(anything()).inAdapterView(withId(R.id.odgovoriPitanja)).atPosition(0).perform(click());
            Thread.sleep(2000);
            String num2 = ((TextView) getCurrentActivity().findViewById(R.id.infBrojPreostalihPitanja)).getText().toString();
            int br2 = Integer.parseInt(num2);
            assertTrue(br1 > br2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void igrajKvizCheckPopUp() {
        try {
            Thread.sleep(50000);
            onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(4).perform(click());
            onData(anything()).inAdapterView(withId(R.id.odgovoriPitanja)).atPosition(0).perform(click());
            Thread.sleep(2000);
            onData(anything()).inAdapterView(withId(R.id.odgovoriPitanja)).atPosition(0).perform(click());
            Thread.sleep(2000);
            onView(withText("Unesite svoje ime:"))
                    .inRoot(withDecorView(not(is(getCurrentActivity().getWindow().getDecorView()))))
                    .check(matches(isDisplayed()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void igrajKvizCheckRangLista() {
        try {
            Thread.sleep(50000);
            onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(4).perform(click());

            onData(anything()).inAdapterView(withId(R.id.odgovoriPitanja)).atPosition(0).perform(click());
            Thread.sleep(2000);

            onData(anything()).inAdapterView(withId(R.id.odgovoriPitanja)).atPosition(0).perform(click());
            //type name
            onView(allOf(
                    isAssignableFrom(EditText.class)))
                    .inRoot(isDialog())
                    .perform(typeText("Test"))
                    .perform(closeSoftKeyboard());
            Thread.sleep(2000);
            onView(withText("OK"))
                    .inRoot(isDialog())
                    .check(matches(isDisplayed()))
                    .perform(click());
            //klick ok
            Thread.sleep(2000);

            onView(withId(R.id.lvRangLista)).check(matches(isDisplayed()));
            String num1 = ((TextView) getCurrentActivity().findViewById(R.id.infBrojPreostalihPitanja)).getText().toString();
            int br1 = Integer.parseInt(num1);
            assertTrue(br1 == 0);
            onView(withId(R.id.lvRangLista)).check(isCompletelyBelow(withId(R.id.informacijePlace)));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void igrajKvizExit() {
        try {
            Thread.sleep(50000);
            onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(4).perform(click());

            onData(anything()).inAdapterView(withId(R.id.odgovoriPitanja)).atPosition(0).perform(click());
            Thread.sleep(2000);

            onView(withId(R.id.btnKraj)).perform(click());
            Thread.sleep(40000);
            onView(withId(R.id.lvKvizovi)).check(matches(isDisplayed()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void igrajKvizEscape() {
        try {
            Thread.sleep(50000);
            onData(anything()).inAdapterView(withId(R.id.lvKvizovi)).atPosition(4).perform(click());

            onData(anything()).inAdapterView(withId(R.id.odgovoriPitanja)).atPosition(0).perform(click());
            Thread.sleep(2000);

            Espresso.pressBack();
            Thread.sleep(40000);
            onView(withId(R.id.lvKvizovi)).check(matches(isDisplayed()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
