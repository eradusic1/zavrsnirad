package ba.unsa.etf.rma;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ba.unsa.etf.rma.aktivnosti.DodajKategorijuAkt;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.TestCase.assertTrue;

@RunWith(AndroidJUnit4.class)
public class DodajKategorijuTest {

    @Rule
    public ActivityTestRule<DodajKategorijuAkt> activityRule
            = new ActivityTestRule<>(DodajKategorijuAkt.class);

    @Test
    public void dodajKategorijuEmpty(){
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Activity aktivnost = ActivityGetter.getCurrentActivity();
        EditText naziv = aktivnost.findViewById(R.id.etNaziv);
        Drawable nazivBackground = naziv.getBackground();
        EditText ikona = aktivnost.findViewById(R.id.etIkona);
        Drawable ikonaBackground = ikona.getBackground();

        onView(withId(R.id.btnDodajKategoriju)).perform(click());
        onView(withId(R.id.btnDodajKategoriju)).check(matches(isDisplayed()));

        EditText naziv2 = aktivnost.findViewById(R.id.etNaziv);
        Drawable nazivBackground2 = naziv2.getBackground();
        EditText ikona2 = aktivnost.findViewById(R.id.etIkona);
        Drawable ikonaBackground2 = ikona2.getBackground();
        assertTrue(!nazivBackground.equals(nazivBackground2));
        assertTrue( !ikonaBackground.equals(ikonaBackground2));
    }

    @Test
    public void dodajKategorijuEmptyIcon(){
        Activity aktivnost = ActivityGetter.getCurrentActivity();
        EditText ikona = aktivnost.findViewById(R.id.etIkona);
        Drawable ikonaBackground = ikona.getBackground();

        onView(withId(R.id.etNaziv)).perform(typeText("Engineer"));
        onView(withId(R.id.btnDodajKategoriju)).perform(click());
        onView(withId(R.id.btnDodajKategoriju)).perform(click());
        onView(withId(R.id.btnDodajKategoriju)).check(matches(isDisplayed()));

        EditText ikona2 = aktivnost.findViewById(R.id.etIkona);
        Drawable ikonaBackground2 = ikona2.getBackground();
        assertTrue( !ikonaBackground.equals(ikonaBackground2));
    }
}
