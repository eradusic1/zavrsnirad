plugins {
   id("com.android.application")
   id("kotlin-android-extensions")
}
android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "ba.unsa.etf.rma"
        minSdkVersion(21)
        targetSdkVersion(23)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
        buildToolsVersion = "29.0.2"
    }
    buildTypes {
        getByName("release") {
            //minifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    productFlavors {
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    // implementation (fileTree(include: ["*.jar"], dir: "libs"))
    implementation("com.android.support:appcompat-v7:28.0.0")
    implementation("com.android.support.constraint:constraint-layout:1.1.3")
    implementation("com.android.support:recyclerview-v7:28.0.0")
    implementation("com.maltaisn:icondialog:2.0.2")
    implementation("com.google.api-client:google-api-client:1.22.0")
    implementation("com.google.api-client:google-api-client-android:1.22.0")
    testImplementation("junit:junit:4.12")
     androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.2") {
        exclude(group = "com.google.code.findbugs", module = "jsr305")
    }
    /*android
            {
                configurations.all
                        {
                            resolutionStrategy.force("com.google.code.findbugs:jsr305:1.3.9")
                        }
            }*/
    androidTestImplementation("com.android.support.test:rules:1.0.2")
    androidTestImplementation("com.android.support.test:runner:1.0.2")
    androidTestImplementation("androidx.test:runner:1.1.0")
}


tasks.register<Exec>("clearData") {
    val adb = android.getAdbExe() as File
    commandLine(adb, "shell", "pm", "clear", "ba.unsa.etf.rma")
    doLast {
        println("clear data done!")
    }
}
tasks.register<Exec>("disableWiFi") {
    val adb = android.getAdbExe() as File
    commandLine(adb, "shell", "svc", "wifi", "disable")
    doLast {
        println("wifi turned OFF!")
    }
}
tasks.register<Exec>("enableWiFi") {
    val adb = android.getAdbExe() as File
    commandLine(adb, "shell", "svc", "wifi", "enable")
    doLast {
        println("wifi turned ON!")
    }
}
tasks.register<Exec>("disableData") {
    val adb = android.getAdbExe() as File
    commandLine(adb, "shell", "svc", "data", "disable")
    doLast {
        println("data turned OFF!")
    }
}
tasks.register<Exec>("enableData") {
    val adb = android.getAdbExe() as File
    commandLine(adb, "shell", "svc", "data", "enable")
    doLast {
        println("data turned ON!")
    }
}
tasks.register<Exec>("turnOffAutomaticRotation") {
    val adb = android.getAdbExe() as File
    commandLine(adb, "shell", "settings", "put", "system", "accelerometer_rotation", 0)
    doLast {
        println("rotation turned off!")
    }
}
tasks.register<Exec>("rotateLandscape") {
    val adb = android.getAdbExe() as File
    commandLine(adb, "shell", "settings", "put", "system", "user_rotation", 1)
    doLast {
        println("rotated landscape!")
    }
}
tasks.register<Exec>("rotatePortrait") {
    val adb = android.getAdbExe() as File
    commandLine(adb, "shell", "settings", "put", "system", "user_rotation", 3)
    doLast {
        println("rotated portrait!")
    }
}
